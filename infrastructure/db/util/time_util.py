# coding: utf-8
import datetime

__author__ = 'syc'


def format_time(time):
    """
    转化时间格式
    """
    end_time = datetime.datetime.fromtimestamp(millis_to_seconds(time)) if time else ""
    return end_time


def millis_to_seconds(millis):
    return round(millis/1000)
