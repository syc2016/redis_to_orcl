# coding: utf-8

from common.pattern.singleton import singleton
from common.log.manager import LogManager
from infrastructure.db.util.time_util import format_time

logger = LogManager.register_log("single_stock_dao")

__author__ = 'syc'


@singleton
class SingleStockDao(object):
    """
    对单只股票策略状态进行持久化操作
    """

    def save_stock_status(self, status, conn):
        """
        插入单只股票策略状态到数据库中

        :param status: 单只股票策略状态
        :param conn: 连接对象

        :raises Exception: save single stock policy status to oracle failed
        """
        sql_insert_stock_policy = (
            "insert into STOCK_POLICY (STOCK_CODE,STOCK_NAME,DIRECTION,QUANTITY,PRICE,EXPECTED_QUANTITY,"
            "MATCHED_QUANTITY,EXPECTED_AMOUNT,MATCHED_AMOUNT,POLICY_ID,POLICY_TYPE,STATE,STATE_ID,PERCENTAGE,"
            "START_TIME,UPDATE_TIME,END_TIME,USER_ID,ACCOUNT_ID,ERR_CODE,ERR_MESSAGE)"
            "VALUES(:stock_code,:stock_name,:direction,:quantity,:price,:expected_quantity,:matched_quantity,"
            ":expected_amount,:matched_amount,:policy_id,:policy_type,:state,:state_id,:percentage,:start_time,"
            ":update_time,:end_time,:user_id,:account_id,:error_code,:error_message)")

        user_id = status['userId'] if status['userId'] else ''
        account_id = status['accountId'] if status['accountId'] else ''

        conn.execute(sql_insert_stock_policy,
                     {'stock_code': status['code'],
                      'stock_name': status['name'],
                      'direction': status['direction'],
                      'quantity': status['quantity'],
                      'price': status['price'],
                      'expected_quantity': status['expectedQuantity'],
                      'matched_quantity': status['matchedQuantity'],
                      'expected_amount': status['expectedAmount'],
                      'matched_amount': status['matchedAmount'],
                      'policy_id': status['policyId'],
                      'policy_type': status['policyType'],
                      'state': status['state'],
                      'state_id': status['state_id'],
                      'percentage': status['percentage'],
                      'start_time': format_time(status['startTime']),
                      'update_time': format_time(status['updateTime']),
                      'end_time': format_time(status['endTime']),
                      'user_id': user_id,
                      'account_id': account_id,
                      'error_code': status['error']['code'],
                      'error_message': status['error']['message']})

    def query_stock_status(self, policy_id, conn):
        """
        查询数据库是否有该policy_id的策略

        :param policy_id: 策略ID
        :param conn: 连接对象

        :return: 表中含有该policy_id记录的条数
        :rtype: int
        """
        sql_query_stock_policy = "select count(POLICY_ID) from STOCK_POLICY where POLICY_ID = :policy_id"
        rows_count = conn.query_count(sql_query_stock_policy, {'policy_id': policy_id})

        return rows_count

    def update_stock_status(self, status, conn):
        """
        更新策略状态

        :param status: 需要更新的策略状态
        :param conn: 连接对象

        :raises Exception: update policy status to oracle failed
        """
        sql_update_stock_policy = ("update STOCK_POLICY set STOCK_NAME=:stock_name,QUANTITY=:quantity,PRICE=:price,"
                                   "EXPECTED_QUANTITY=:expected_quantity,MATCHED_QUANTITY=:matched_quantity,"
                                   "EXPECTED_AMOUNT=:expected_amount,MATCHED_AMOUNT=:matched_amount,STATE=:state,"
                                   "STATE_ID=:state_id,PERCENTAGE=:percentage,UPDATE_TIME=:update_time,"
                                   "END_TIME=:end_time,ERR_CODE=:error_code,ERR_MESSAGE=:error_message "
                                   "where POLICY_ID=:policy_id")

        conn.execute(sql_update_stock_policy,
                     {'stock_name': status['name'], 'quantity': status['quantity'], 'price': status['price'],
                      'state': status['state'],
                      'expected_quantity': status['expectedQuantity'],
                      'matched_quantity': status['matchedQuantity'],
                      'expected_amount': status['expectedAmount'], 'matched_amount': status['matchedAmount'],
                      'state_id': status['state_id'], 'percentage': status['percentage'],
                      'update_time': format_time(status['updateTime']),
                      'end_time': format_time(status['endTime']), 'error_code': status['error']['code'],
                      'error_message': status['error']['message'], 'policy_id': status['policyId']})
