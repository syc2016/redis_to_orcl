# coding: utf-8

from common.pattern.singleton import singleton
from common.log.manager import LogManager
from infrastructure.db.util.time_util import format_time

logger = LogManager.register_log("custom_basket_dao")

__author__ = 'syc'


@singleton
class CustomBasketDao(object):
    """
    对custom_basket_policy、custom_basket_item表进行持久化操作
    """

    def save_basket_status(self, status, conn):
        """
        插入篮子策略状态到数据库中

        :param status: 篮子策略
        :param conn: 连接对象

        :raises Exception: save policy status to oracle failed
        """
        sql_insert_basket_policy = ("insert into CUSTOM_BASKET_POLICY (BASKET_CODE,BASKET_NAME,DIRECTION,"
                                    "VOLUME,MATCHED_IOPV, EXPECTED_QUANTITY,MATCHED_QUANTITY, "
                                    "EXPECTED_AMOUNT,MATCHED_AMOUNT,PAIR_POLICY_ID,POLICY_ID,POLICY_TYPE,STATE,"
                                    "STATE_ID,PERCENTAGE,START_TIME,UPDATE_TIME,END_TIME,"
                                    "USER_ID,ACCOUNT_ID,ERR_CODE,ERR_MESSAGE) "
                                    "VALUES(:code,:basket_name,:direction,:volume,:matched_iopv,:expected_quantity,"
                                    ":matched_quantity,:expected_amount,:matched_amount,:pair_policy_id,:policy_id,"
                                    ":policy_type,:state,:state_id,:percentage,:start_time,:update_time,:end_time,"
                                    ":user_id,:account_id,:error_code,:error_message)")

        user_id = status['userId'] if status['userId'] else ''
        account_id = status['accountId'] if status['accountId'] else ''

        conn.execute(sql_insert_basket_policy,
                     {'code': status['code'], 'basket_name': status['name'], 'direction': status['direction'],
                      'volume': status['volume'], 'matched_iopv': status['matchedIopv'],
                      'expected_quantity': status['expectedQuantity'],
                      'matched_quantity': status['matchedQuantity'],
                      'expected_amount': status['expectedAmount'], 'matched_amount': status['matchedAmount'],
                      'pair_policy_id': status['pairPolicyId'], 'policy_id': status['policyId'],
                      'policy_type': status['policyType'], 'state': status['state'],
                      'state_id': status['state_id'], 'percentage': status['percentage'],
                      'start_time': format_time(status['startTime']),
                      'end_time': format_time(status['endTime']),
                      'update_time': format_time(status['updateTime']),
                      'user_id': user_id, 'account_id': account_id,
                      'error_code': status['error']['code'], 'error_message': status['error']['message']})

    def query_basket_status(self, policy_id, conn):
        """
        查询数据库是否有该policy_id的策略

        :param policy_id: 篮子策略ID
        :param conn: 连接对象

        :return: 表中含有该policy_id记录的条数
        :rtype: int
        """
        sql_query_basket_policy = "select count(POLICY_ID) from CUSTOM_BASKET_POLICY where POLICY_ID = :policy_id"
        rows_count = conn.query_count(sql_query_basket_policy, {'policy_id': policy_id})

        return rows_count

    def update_basket_status(self, status, conn):
        """
        更新策略状态

        :param status: 需要更新的策略状态
        :param conn: 连接对象

        :raises Exception: update policy status to oracle failed
        """
        sql_update_basket_policy = ("update CUSTOM_BASKET_POLICY set BASKET_NAME=:basket_name,VOLUME=:volume,"
                                    " MATCHED_IOPV=:matched_iopv,EXPECTED_QUANTITY=:expected_quantity,"
                                    "MATCHED_QUANTITY=:matched_quantity,EXPECTED_AMOUNT=:expected_amount,"
                                    "MATCHED_AMOUNT=:matched_amount,PAIR_POLICY_ID=:pair_policy_id,"
                                    "STATE=:state,STATE_ID=:state_id,PERCENTAGE=:percentage,UPDATE_TIME=:update_time,"
                                    "END_TIME=:end_time,ERR_CODE=:error_code,ERR_MESSAGE=:error_message "
                                    "where POLICY_ID=:policy_id")

        conn.execute(sql_update_basket_policy,
                     {'basket_name': status['name'], 'volume': status['volume'], 'matched_iopv': status['matchedIopv'],
                      'expected_quantity': status['expectedQuantity'],
                      'matched_quantity': status['matchedQuantity'],
                      'expected_amount': status['expectedAmount'], 'matched_amount': status['matchedAmount'],
                      'pair_policy_id': status['pairPolicyId'], 'state': status['state'],
                      'state_id': status['state_id'], 'percentage': status['percentage'],
                      'update_time': format_time(status['updateTime']),
                      'end_time': format_time(status['endTime']), 'error_code': status['error']['code'],
                      'error_message': status['error']['message'], 'policy_id': status['policyId']})

    def save_item_status(self, item_status, policy_id, conn):
        """
        插入篮子成分股信息

        :param item_status: 单只成分股信息
        :param policy_id: 对应的篮子策略ID
        :param conn: 连接对象

        :raises Exception: save item status to oracle failed
        """
        sql_insert_basket_item = ("insert into CUSTOM_BASKET_ITEM (STOCK_CODE,QUANTITY,ORDER_PRICE,EXPECTED_QUANTITY,"
                                  "MATCHED_QUANTITY,EXPECTED_AMOUNT,MATCHED_AMOUNT,STATE,STATE_ID,ERR_CODE,ERR_MESSAGE,"
                                  "POLICY_ID) "
                                  "VALUES(:code,:quantity,:order_price,:expected_quantity,:matched_quantity,"
                                  ":expected_amount,:matched_amount,:state,:state_id,:error_code,:error_message,"
                                  ":policy_id)")
        cursor = conn.conn.cursor()
        try:
            cursor.prepare(sql_insert_basket_item)
        except Exception as e:
            logger.exception('Failed to prepare cursor')
            raise Exception("Failed to prepare cursor %s" % e)

        array = []
        for item in item_status:
            array.append((item['code'], item['quantity'], item['orderPrice'], item['expectedQuantity'],
                          item['matchedQuantity'], item['expectedAmount'], item['matchedAmount'], item['state'],
                          item['state_id'], item['error']['code'], item['error']['message'], policy_id))

        try:
            cursor.executemany(sql_insert_basket_item, array)
        except Exception as e:
            logger.exception('Failed to insert rows')
            raise Exception("Failed to prepare cursor %s" % e)

    def query_item_status(self, policy_id, item_code, conn):
        """
        查询表custom_basket_item中该policy_id是否包含该成分股信息

        :param policy_id: 篮子策略ID
        :param item_code: 成分股code
        :param conn: 连接对象
        :return: 表中含有该policy_id记录的条数
        :rtype: int
        """
        sql_query_basket_item = ('select count(*) from CUSTOM_BASKET_ITEM where POLICY_ID = :policy_id '
                                 'and STOCK_CODE=:code')
        rows_count = conn.query_count(sql_query_basket_item, {'policy_id': policy_id, 'code': item_code})
        return rows_count

    def update_item_status(self, item_status, policy_id, conn):
        """
        更新成分股信息

        :param item_status: 单只成分股状态信息
        :param policy_id: 对应的篮子策略ID
        :param conn: 连接对象

       :raises Exception: update item status to oracle failed
        """
        sql_update_basket_item = ("update CUSTOM_BASKET_ITEM set EXPECTED_QUANTITY=:expected_quantity,"
                                  "MATCHED_QUANTITY=:matched_quantity,EXPECTED_AMOUNT=:expected_amount,"
                                  "MATCHED_AMOUNT=:matched_amount,STATE=:state,STATE_ID=:state_id,ERR_CODE=:error_code,"
                                  "ERR_MESSAGE=:error_message where POLICY_ID = :policy_id and STOCK_CODE=:code")
        cursor = conn.conn.cursor()
        try:
            cursor.prepare(sql_update_basket_item)
        except Exception as e:
            logger.exception('Failed to prepare cursor')
            raise Exception("Failed to prepare cursor %s" % e)

        array = []
        for item in item_status:
            array.append((item['expectedQuantity'], item['matchedQuantity'], item['expectedAmount'],
                          item['matchedAmount'], item['state'], item['state_id'], item['error']['code'],
                          item['error']['message'], policy_id, item['code']))

        try:
            cursor.executemany(sql_update_basket_item, array)
        except Exception as e:
            logger.exception('Failed to insert rows')
            raise Exception("Failed to prepare cursor %s" % e)
