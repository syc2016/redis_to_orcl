# coding: utf-8
from apscheduler.schedulers.blocking import BlockingScheduler
from common.log.manager import LogManager
from infrastructure.db.config import POLICY_ID_STATUS
logger = LogManager.register_log("clear_cache")

__author__ = 'syc'


def tick():
    POLICY_ID_STATUS.clear()


def start_scheduler():
    scheduler = BlockingScheduler()
    scheduler.add_job(tick, 'cron', minute='30', hour='12,18', day_of_week='0-4')
    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit) as e:
        scheduler.shutdown()
        logger.error('clear policy status scheduler start exception: %s' % repr(e))
