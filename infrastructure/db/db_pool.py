# coding: utf-8
from common.database.ora_db_helper import OraDbHelper
from common.log.manager import LogManager
logger = LogManager.register_log("db_pool")

__author__ = 'syc'


class DbThreadPool(object):
    """
    线程池

    Attributes:
        ora_user: 连接数据库的用户名
        ora_pwd: 连接数据库的用户名密码
        ora_ip: 数据库IP
        ora_port: 数据库的端口
        ora_sid: 数据库的一个实例名
        size: 数据库连接池初始化的连接个数
    """
    def __init__(self, ora_user, ora_pwd, ora_ip, ora_port, ora_sid, size=2):
        self.conn_pool = []  # (list) 元素类型：(dict) key: conn 每个数据库连接 value: True or False 该连接是否忙碌
        self.size = size  # 初始化连接池大小
        self._init_conn_pool(ora_user, ora_pwd, ora_ip, ora_port, ora_sid, self.size)

    def __repr__(self):
        return type(self).__name__

    def _init_conn_pool(self, ora_user, ora_pwd, ora_ip, ora_port, ora_sid, size):
        """
        初始化连接池

        :param size: 初始化连接池的大小
        :return:
        """
        for _ in xrange(size):
            conn = OraDbHelper()
            conn.connect(ora_user, ora_pwd, ora_ip, ora_port, ora_sid)
            conn_dict = {'conn': conn, 'busy': False}
            self.conn_pool.append(conn_dict)

    def get_connect(self):
        """
        取得数据库连接

        :return: conn
        :rtype common.database.ora_db_helper.OraDbHelper
        """

        # valid_conn_list = filter(lambda conn_dict: conn_dict['conn'].is_alive() and not conn_dict['busy'],
        #                          self.conn_pool)

        valid_conn_list = filter(lambda conn_item: conn_item['busy'] is False, self.conn_pool)
        if valid_conn_list:
            conn_dict = valid_conn_list[0]
            conn = conn_dict['conn']
            conn_dict['busy'] = True
        else:
            logger.error('%s error: db thread pools not exists connected conn or all conn is busy' % repr(self))
            raise Exception('%s error: db thread pools not exists connected conn or all conn is busy' % repr(self))

        return conn
