# coding: utf-8
import Queue

__author__ = 'syc'

SING_STOCK_QUEUE = Queue.Queue()  # insert policy queue
BASKET_QUEUE = Queue.Queue()  # insert policy queue
NOTIFY_QUEUE = Queue.Queue()  # notify queue

TYPE_STOCK_SINGLE = 0  # 单只股票
TYPE_STOCK_BASKET = 1  # 篮子

POLICY_ID_STATUS = dict()  # (dict) key: policy_id, value: policy_status
