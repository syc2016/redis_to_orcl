#!/usr/bin/env python
# coding: utf-8
import yaml

from common.redis_tool.redis_tool import RedisTool
from common.log.config import LogConfig
from common.log.manager import LogManager

from infrastructure.db.clear_cache import start_scheduler
from infrastructure.db.config import BASKET_QUEUE
from infrastructure.db.config import SING_STOCK_QUEUE
from infrastructure.db.config import NOTIFY_QUEUE
from infrastructure.db.policy_status_consumer import BasketConsumer
from infrastructure.db.policy_status_consumer import NotifyThread
from infrastructure.db.policy_status_consumer import SingleStockConsumer

__author__ = 'syc'
VERSION = "V1.0.2"
# 读取配置
config_file = open("configs.yaml")
Load_configs = yaml.load(config_file)

redis_config = Load_configs["redis"]
redis_ip = redis_config["ip"]
redis_port = redis_config["port"]

orcl = Load_configs['orcl']
orcl_user = orcl['user']
orcl_pwd = orcl['pwd']
orcl_ip = orcl['ip']
orcl_port = orcl['port']
orcl_sid = orcl['sid']

per_redis_data_size = Load_configs['per_redis_data_size']
pool_thread_size = Load_configs['pool_thread_size']
policy_list_key = Load_configs['policy_list_key']

Log_Config = Load_configs['log']
LogConfig.LOG_TO_FILE = True
LogConfig.LOG_DIR = "Logs"
LogConfig.LOG_FILE_MAX_SIZE = Log_Config["log_file_max_size"]
LogConfig.LOG_LEVEL_CONSOLE = Log_Config["log_level_console"]
LogConfig.LOG_LEVEL_FILE = Log_Config["log_level_file"]
LogConfig.LOG_NAME = "redis_to_orcl_app"
LogManager.init()


def main():
    print VERSION
    from infrastructure.db.db_pool import DbThreadPool
    from infrastructure.db.policy_status_producer import RedisPolicyStatus

    redis_tool = RedisTool(redis_ip, redis_port)
    redis_producer = RedisPolicyStatus(redis_tool, per_redis_data_size, policy_list_key)
    redis_producer.start()  # 启动从redis读取数据线程

    db_pool = DbThreadPool(orcl_user, orcl_pwd, orcl_ip, orcl_port, orcl_sid, pool_thread_size)  # 初始化数据库连接池

    conn = db_pool.get_connect()
    basket_consumer = BasketConsumer(conn, BASKET_QUEUE)
    basket_consumer.start()

    conn = db_pool.get_connect()
    single_stock_consumer = SingleStockConsumer(conn, SING_STOCK_QUEUE)
    single_stock_consumer.start()

    notify_thread = NotifyThread(redis_producer, NOTIFY_QUEUE)
    notify_thread.start()

    start_scheduler()  # 开启定时任务删除缓存POLICY_ID_STATUS


if __name__ == '__main__':
    main()
