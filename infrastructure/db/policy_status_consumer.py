# coding: utf-8
import threading

from common.log.manager import LogManager
from infrastructure.db.config import BASKET_QUEUE
from infrastructure.db.config import NOTIFY_QUEUE
from infrastructure.db.config import SING_STOCK_QUEUE
from infrastructure.db.config import POLICY_ID_STATUS
from infrastructure.db.dao.custom_basket_dao import CustomBasketDao
from infrastructure.db.dao.single_stock_dao import SingleStockDao

logger = LogManager.register_log("policy_status_consumer")

__author__ = 'syc'


class NotifyThread(threading.Thread):
    """
    通知可以继续从redis读取数据

    Attributes:
        redis_producer: 从redis读取对象实例
        queue: 通知队列
    """
    def __init__(self, redis_producer, queue):
        super(NotifyThread, self).__init__()
        self.redis_producer = redis_producer
        self.queue = queue

    def run(self):
        while True:
            func = NOTIFY_QUEUE.get()
            if func():
                self.queue.queue.clear()
                self.redis_producer.update()


class Consumer(threading.Thread):
    """
    消费从redis中获取的数据

    Attributes:
        conn: 数据库的连接
        queue: 需要消费的队列
    """
    def __init__(self, conn, queue):
        super(Consumer, self).__init__()
        self.queue = queue
        self.conn = conn

    def run(self):
        status_list = []
        while True:
            try:
                if not status_list:
                    self.conn.start_transaction()
                status = self.queue.get()
                status_list.append(status)
                self.get_policy_status(status)

                if self.queue.empty():
                    del status_list[:]
                    self.conn.commit()
                    NOTIFY_QUEUE.put(self.notify)
            except Exception as e:
                self.conn.rollback()
                map(lambda item: self.queue.put(item), status_list)
                logger.exception("write policy status to oracle failed! error: %s" % e)

    def notify(self):
        flag = False
        if SING_STOCK_QUEUE.empty() and BASKET_QUEUE.empty():
            flag = True

        return flag


class BasketConsumer(Consumer):
    """
    将策略状态队列中的策略状态进行入库
    """

    def __init__(self, conn, queue):
        super(BasketConsumer, self).__init__(conn, queue)
        self.db_dao = CustomBasketDao()

    def get_policy_status(self, status):
        """
        插入或更新篮子策略状态信息
        """
        policy_id = status['policyId']
        if policy_id not in POLICY_ID_STATUS:
            row_count = self.db_dao.query_basket_status(policy_id, self.conn)
        else:
            row_count = 1

        policy_operation_type = 'create'
        if row_count:
            policy_operation_type = 'update'

        item_operation_type = ''
        item_status = status['itemStatus']
        if item_status:
            item_operation_type = 'create'
            previous_status = POLICY_ID_STATUS.get(policy_id, None)
            if previous_status and previous_status['itemStatus']:
                item_status = filter(lambda item: self.filter_item(item, previous_status['itemStatus']), item_status)
                item_operation_type = 'update'
            else:
                item_row_count = self.db_dao.query_item_status(policy_id, item_status[0]['code'], self.conn)
                if item_row_count:  # 策略状态为create时没有成分股信息，查询custom_policy_item表判断create or update
                    item_operation_type = 'update'

        POLICY_ID_STATUS[policy_id] = status
        self.execute_sql(policy_operation_type, item_operation_type, status, item_status)

    def execute_sql(self, policy_operation_type, item_operation_type, status, item_status):
        """
        执行篮子策略对数据库的操作
        :param policy_operation_type: 对custom_basket_policy操作符
        :param item_operation_type: 对custom_basket_item操作符
        :param status: 篮子策略状态
        :param item_status: 篮子成分股状态
        """
        if policy_operation_type == 'create':
            self.db_dao.save_basket_status(status, self.conn)
        elif policy_operation_type == 'update':
            self.db_dao.update_basket_status(status, self.conn)

        if item_operation_type == 'create':
            self.db_dao.save_item_status(item_status, status['policyId'], self.conn)
        elif item_operation_type == 'update':
            self.db_dao.update_item_status(item_status, status['policyId'], self.conn)

    def filter_item(self, item, previous_item_status):
        """
        过滤不需要进行更新的成分股状态

        :param item: 最新策略状态的某只成分股
        :param previous_item_status: 上一个存储的同一策略状态的所有成分股
        """
        code = item['code']
        state_id = item['state_id']
        for previous_item in previous_item_status:
            if previous_item['code'] == code and previous_item['state_id'] != state_id:
                return item


class SingleStockConsumer(Consumer):
    """
    将策略状态队列中的策略状态进行入库
    """
    def __init__(self, conn, queue):
        super(SingleStockConsumer, self).__init__(conn, queue)
        self.db_dao = SingleStockDao()

    def get_policy_status(self, status):
        """
        插入或更新单只股票策略状态信息
        """
        policy_id = status['policyId']
        if policy_id not in POLICY_ID_STATUS:
            row_count = self.db_dao.query_stock_status(policy_id, self.conn)
        else:
            row_count = 1

        policy_operation_type = 'create'
        if row_count:
            policy_operation_type = 'update'

        POLICY_ID_STATUS[policy_id] = status
        self.execute_sql(policy_operation_type, status)

    def execute_sql(self, policy_operation_type, status):
        """
        执行单只股票策略对数据库的操作
        :param policy_operation_type: 对stock_policy操作符
        :param status: 单只股票策略状态
        """
        if policy_operation_type == 'create':
            self.db_dao.save_stock_status(status, self.conn)
        elif policy_operation_type == 'update':
            self.db_dao.update_stock_status(status, self.conn)
