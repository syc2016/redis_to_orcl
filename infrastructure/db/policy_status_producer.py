# coding: utf-8
import json
import threading
import time
from common.log.manager import LogManager
from infrastructure.db.config import TYPE_STOCK_SINGLE
from infrastructure.db.config import SING_STOCK_QUEUE
from infrastructure.db.config import BASKET_QUEUE
from infrastructure.db.config import TYPE_STOCK_BASKET
from infrastructure.db.config import POLICY_ID_STATUS

logger = LogManager.register_log("policy_status_producer")

__author__ = 'syc'


class RedisPolicyStatus(threading.Thread):
    """
    从redis读取数据存入插入策略队列和更新策略队列的线程

    Attributes:
        redis_tool: 操作redis的引用
        max_length: 每次从redis取的数据量
        redis_key: redis key
    """
    POLICY_TYPE_MAP = {TYPE_STOCK_SINGLE: SING_STOCK_QUEUE,
                       TYPE_STOCK_BASKET: BASKET_QUEUE}

    def __init__(self, redis_tool, max_length, redis_key):
        super(RedisPolicyStatus, self).__init__()
        self.redis_tool = redis_tool
        self.max_length = max_length
        self.redis_key = redis_key

        self.redis_data_length = 0  # 每次从redis取得的数据量
        self.start_time = 0.0

    def run(self):
        """
        重写线程的run方法
        """
        while True:
            policy_id_list = self.redis_tool.get_list_range(self.redis_key, 0, self.max_length-1)
            if policy_id_list:
                break
            threading._sleep(0.1)

        self.start_time = time.time()
        self.redis_data_length = len(policy_id_list)
        logger.info('from redis get data length: %d' % self.redis_data_length)
        non_repeat_policy = sorted(set(policy_id_list), key=policy_id_list.index)
        return_list = map(lambda policy_id: self.put_status_queue(policy_id), non_repeat_policy)
        if return_list.count(None) == len(return_list):
            self.update()

    def put_status_queue(self, policy_id):
        """
        从redis拿到的数据放入队列中

        :param policy_id: 策略状态信息有变化的策略 policy_id
        """
        key = "PS.%s" % policy_id
        status_json_str = self.redis_tool.get(key)
        if status_json_str:
            status = json.loads(status_json_str)  # 将json str转为dict
            if policy_id not in POLICY_ID_STATUS\
                    or (policy_id in POLICY_ID_STATUS and POLICY_ID_STATUS[policy_id]['state_id'] < status['state_id']):
                self.POLICY_TYPE_MAP[status['policyType']].put(status)

                return policy_id
        else:
            logger.error('redis have not this policy_id: %s data' % policy_id)
            raise Exception('redis have not this policy_id: %s data' % policy_id)

    def update(self):
        """
        删除已经从redis取得的数据，继续从redis取数据
        :return:
        """
        logger.info('%d data consume time: %s s' % (self.redis_data_length, time.time() - self.start_time))
        self.redis_tool.list_trim(self.redis_key, self.redis_data_length, -1)
        self.run()
