__author__ = 'wangshuo'

from handler_thread import HandlerThread


class ThreadPool(object):

    cur_index = 0
    thread_pool = []
    for i in range(100):
        thread = HandlerThread()
        thread_pool.append(thread)
        thread.start()

    @staticmethod
    def gain_thread():
        ThreadPool.cur_index += 1
        if ThreadPool.cur_index >= len(ThreadPool.thread_pool):
            ThreadPool.cur_index = 0
        return ThreadPool.thread_pool[ThreadPool.cur_index]

    @staticmethod
    def destroy():
        for thread in ThreadPool.thread_pool:
            thread.stop()