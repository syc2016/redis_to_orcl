#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'


class CoroutineConfig(object):
    """协程模块相关配置

    CONFIG_HANDLER_THREAD_RUNNING_COUNT_PER_SCHEDULE：用于HandlerThread一次调度能够运行的Runnable个数，
    能够有限的提高性能。这个值不宜设置过高，会影响并发性。
    """
    CONFIG_HANDLER_THREAD_RUNNING_COUNT_PER_SCHEDULE = 5
