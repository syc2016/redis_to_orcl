#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
import threading
from Queue import Queue
from handler_thread import Runnable
"""
注意该基础模块，如果与gevent同时使用，需要打上gevnet补丁，
gevent.monkey.patch_thread()-------为了OriginHandlerThread,
后来测试发现不需要打patch_thread(）依然可以正常使用，而打了patch_thread
反而调试无法一步步往下执行

OriginTimer 与gevent存在部分冲突地方，目前系统中暂未使用，采用
gevent中的timer进行替代使用
"""


class OriginHandlerThread(object):
    """
    python线程级别的handleThread
    """
    def __init__(self):
        self._started = False
        self._running = False
        self._completed = False
        self._internal_queue = None
        self.thread = threading.Thread(target=self._run)
        self.thread.setDaemon(True)

    def started(self):
        return self._started

    def start(self):
        """启动HandlerThread。启动后才能Post等操作，一个HandlerThread只能启动一次。"""
        if self._started:
            raise Exception('OriginHandlerThread already started!')
        self._started = True
        self._internal_queue = Queue()
        self.thread.start()

    def _run(self):
        while True:
            runnable = self._internal_queue.get()
            # runnable为None，用于表示退出操作。这时，执行循环结束，当前的协程结束，无法继续使用
            if runnable is None:
                self._running = False
                break
            # 执行目标Runnable
            runnable.run()

    def post(self, runnable):
        # 没启动无法执行
        if not self._started:
            raise Exception('OriginHandlerThread : post : OriginHandlerThread not started!')
        if self._completed:
            raise Exception('OriginHandlerThread : post : OriginHandlerThread already completed!')
        if not isinstance(runnable, Runnable):
            raise TypeError('OriginHandlerThread : post : [%s] should be a Runnable!' % runnable)
        self._internal_queue.put(runnable)

    def post_delayed(self, runnable, timeout):
        raise AssertionError('post_delayed not support, please use Timer instead.')

    def clear(self):
        # 没启动无法执行
        if not self._started:
            raise Exception('OriginHandlerThread : clear : OriginHandlerThread not started!')
        if self._completed:
            raise Exception('OriginHandlerThread : clear : OriginHandlerThread already complated!')
        while not self._internal_queue.empty():
            self._internal_queue.get()

    def stop(self):
        """结束当前的HandlerThread。Stop操作将会导致HandlerThread无法继续使用。"""
        # 没启动无法执行
        if not self._started:
            raise Exception('OriginHandlerThread : stop : OriginHandlerThread not started!')
        # 已经结束了
        if self._completed:
            return
        # 我们需要在stop的时候，清空消息队列。
        self.clear()
        # 设置结束态，后续将无法使用
        self._completed = True
        # 通过发起一个None消息，实现了退出功能。
        self._internal_queue.put(None)


class OriginTimer(object):
    """
    根据python线程定时器的封装使用
    """
    def __init__(self, delay):
        self._delay = delay
        self._timer = None

    def reset(self, delay=None):
        self.stop()
        if delay is not None:
            self._delay = delay

    def post(self, runnable):
        self._timer = threading.Timer(self._delay, runnable.run)
        self._timer.start()

    def stop(self):
        """
        该方法只能停止还在等待的定时器，若方法已经正在执行，
        调用内部cancel无效
        :return:
        """
        self._timer.cancel()
        self._timer = None





