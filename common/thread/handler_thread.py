# coding=utf-8
__author__ = 'wangshuo'

import gevent
from gevent.queue import Queue
from gevent.event import Event
from common.log.manager import LogManager
from config import CoroutineConfig
logger = LogManager.register_log("handler_thread")

# 用于统计程序中HandleThread运行的数量
HANDLE_THREAD_RUNNING_COUNT = {"count": 0}


class HandlerThread(object):
    """HandlerThread封装了gevent。相当于一个可执行的线程。通过post来达到线程间消息通讯。"""

    def __init__(self):
        self.__started = False
        self.__running = False
        self.__completed = False
        self.__wait_event = Event()
        self._internal_queue = None

    def started(self):
        return self.__started

    def start(self):
        """启动HandlerThread。启动后才能Post等操作，一个HandlerThread只能启动一次。"""
        if self.__started:
            raise Exception('HandlerThread already started!')
        self.__started = True
        self._internal_queue = Queue()
        gevent.spawn(self._run)

    def __del__(self):
        logger.debug('HandlerThread __del__...')

    def _run(self):
        global GLOBLE_HANDLE_THREAD_RUNNING_COUNT
        HANDLE_THREAD_RUNNING_COUNT["count"] += 1
        cur_index = 0
        while True:
            runnable = self._internal_queue.get()
            # runnable为None，用于表示退出操作。这时，执行循环结束，当前的协程结束，无法继续使用
            if runnable is None:
                self.__running = False
                logger.info('Exit HandlerThread.')
                self.__wait_event.set()
                break
            # logger.debug(runnable)
            # 执行目标Runnable
            try:
                runnable.run()
            except Exception, e:
                logger.exception("%s:_run:UnKnown Error.Must be careful.Error:%s" % (self.__class__.__name__, e))
                break
            # 通知waitdone
            if self._internal_queue.empty():
                self.__wait_event.set()
            # 这里sleep操作转让了执行，让其他HandlerThread的消息队列调度起来
            cur_index += 1
            if cur_index >= CoroutineConfig.CONFIG_HANDLER_THREAD_RUNNING_COUNT_PER_SCHEDULE:
                gevent.sleep(0)
                cur_index = 0
        HANDLE_THREAD_RUNNING_COUNT["count"] -= 1

    def post(self, runnable):
        # 没启动无法执行
        # if not self.__started:
        #     raise Exception('HandlerThread : post : HandlerThread not started!')
        # if self.__completed:
        #     raise Exception('HandlerThread : post : HandlerThread already completed!')
        # if not isinstance(runnable, Runnable):
        #     raise TypeError('HandlerThread : post : [%s] should be a Runnable!' % runnable)
        self._internal_queue.put(runnable)

    def post_delayed(self, runnable, timeout):
        raise AssertionError('post_delayed not support, please use Timer instead.')

    def clear(self):
        # 没启动无法执行
        if not self.__started:
            raise Exception('HandlerThread : clear : HandlerThread not started!')
        if self.__completed:
            raise Exception('HandlerThread : clear : HandlerThread already complated!')
        while not self._internal_queue.empty():
            self._internal_queue.get()

    def stop(self):
        """结束当前的HandlerThread。Stop操作将会导致HandlerThread无法继续使用。"""
        # 没启动无法执行
        if not self.__started:
            raise Exception('HandlerThread : stop : HandlerThread not started!')
        # 已经结束了
        if self.__completed:
            return
        # 我们需要在stop的时候，清空消息队列。
        self.clear()
        # 设置结束态，后续将无法使用
        self.__completed = True
        # 通过发起一个None消息，实现了退出功能。
        self._internal_queue.put(None)

    def wait_done(self):
        # 没启动无法执行
        if not self.__started:
            raise Exception('HandlerThread not started!')
        # 无任务，结束
        if self._internal_queue.empty():
            return
        self.__wait_event = Event()
        self.__wait_event.wait()


class Timer(object):
    """
    使用gevent内置的Timer。
    这里注意timer中执行的方法必须放在gevent.spawn中或者handler_thread中执行
    """
    def __init__(self, delay, repeat=0, target_thread=None):
        self._delay = delay
        self._repeat = repeat
        self._target_thread = target_thread
        self.loop = gevent.get_hub().loop
        self._timer = self.loop.timer(self._delay, self._repeat)

    def reset(self, delay=None, repeat=None):
        self.stop()
        if delay is not None:
            self._delay = delay
        if repeat is not None:
            self._repeat = repeat
        self._timer = self.loop.timer(self._delay, self._repeat)

    def _run(self, runnable):
        if self._target_thread is not None:
            self._target_thread.post(runnable)
        else:
            gevent.spawn(runnable.run)

    def post(self, runnable):
        self.reset()
        self._timer.start(self._run, runnable)

    def stop(self):
        if self._timer is not None:
            self._timer.stop()
            self._timer = None


class Runnable(object):
    __slots__ = ("func", "args", "kwargs")
    """Runnable是HandlerThread的执行单元。"""
    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def run(self):
        self.func(*self.args, **self.kwargs)

