#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
import time
from gevent.event import Event


class ProxyCacheFactory(object):
    # key:(func_id,params),value:ProxyCache
    proxy_caches = {}

    @staticmethod
    def get_proxy_cache(func_id, delay_time, rpc_func, *param_list):
        key = (func_id, param_list)
        if key in ProxyCacheFactory.proxy_caches:
            return ProxyCacheFactory.proxy_caches[key]
        else:
            proxy_cache = ProxyCache(delay_time, rpc_func, *param_list)
            ProxyCacheFactory.proxy_caches[key] = proxy_cache
            return proxy_cache


class ProxyCache(object):
    def __init__(self, delay_time, rpc_func, *param_list):
        self.object = None
        self.time = None
        self.delay_time = delay_time
        self.rpc_func = rpc_func
        self.rpc_params = param_list
        self.lock = None

    def get_cache(self):
        if self.time is not None:
            now = time.time()
            if (now - self.time) < self.delay_time:
                return self.object
        if self.lock is None:
            self.lock = Event()
            if self.rpc_params == (None,):
                obj = self.rpc_func()
            else:
                obj = self.rpc_func(*self.rpc_params)
            self.lock.set()
            self.lock = None
            self.object = obj
            self.time = time.time()
        else:
            self.lock.wait()
        return self.object


