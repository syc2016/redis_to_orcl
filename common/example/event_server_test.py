#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.event.domain_event import DomainEvent
from common.event.event_publisher import DomainEventPublisher
import json
import time
from common.redis_tool.redis_tool import RedisTool


class OrderEvent(DomainEvent):
    def __init__(self):
        super(OrderEvent, self).__init__()
        self.order_no = ""
        self.order_price = 0
        self.order_num = 0

    def on_child_serialize(self):
        self.body["orderNo"] = self.order_no
        self.body["orderPrice"] = self.order_price
        self.body["orderNum"] = self.order_num
        # return json.dumps(self.body)
        return self.body

redis_tool = RedisTool("10.10.1.29", 7000)
event_publisher = DomainEventPublisher(redis_tool, "orders")

count = 0
while True:
    count += 1
    stock_order = OrderEvent()
    stock_order.id = count
    stock_order.type_name = "StockOrder"
    stock_order.order_no = "NO%s" % count
    stock_order.order_price = count * 10
    stock_order.order_num = 100 * count
    event_publisher.publish(stock_order)
    print event_publisher.msg_id
    time.sleep(1)