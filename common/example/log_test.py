#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.log.config import LogConfig, LogLevel
from common.log.manager import LogManager
import sys, os


LogConfig.LOG_TO_FILE = True
LogConfig.LOG_NAME = "log_test"
LogConfig.LOG_LEVEL_FILE = LogLevel.ERROR
LogConfig.LOG_LEVEL_CONSOLE = LogLevel.DEBUG
if getattr(sys, 'frozen', None):
    path = os.path.dirname(sys.executable)
    LogConfig.LOG_DIR = os.path.join(path, os.curdir, "Logs")
else:
    path = os.path.dirname(__file__)
    LogConfig.LOG_DIR = os.path.join(path, os.pardir, "Logs")

LogManager.init()
logger = LogManager.register_log("log_test", LogLevel.DEBUG)
logger.info("wo kai shi")
logger.debug("wo debug")
logger.error("wo error")
logger1 = LogManager.register_log("log_test1", LogLevel.INFO)
logger1.info("logger1 kai shi")



