#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.event.event_listener import DomainEventListener
from common.redis_tool.redis_tool import RedisTool
import threading
import time


class OrderListener(DomainEventListener):
    def filter_dispatch(self, event):
        print("id:%s,type:%s,occurred_on:%s,body:%s" % (event.id, event.type_name, event.occurred_on, event.body))
        print "body:orderNo:%s" % event.body["orderNo"]
        print "orderPrice:%s" % event.body["orderPrice"]
        print "orderNum:%s" % event.body["orderNum"]
        return True


redis_tool = RedisTool("10.10.1.29", 7000)
event_listener = OrderListener(redis_tool, "orders", "robot", ["StockOrder"])

def un_subscribe():
    time.sleep(15)
    print("unlisten")
    event_listener.stop()

thread = threading.Thread(target=un_subscribe)
thread.setDaemon(True)
thread.start()

event_listener.listen()





