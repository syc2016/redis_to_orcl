﻿#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
"""
使用单机redis版本可以仅仅安装redis2.10.5包
使用集群redis版本可以仅仅安装redis-py-cluster1.2.0包
"""
try:
    import redis
except:
    pass
try:
    import rediscluster
except:
    pass


class RedisEnvironmentException(Exception):
    """缺少redis相关依赖环境时候引发的异常"""
    def __init__(self, is_redis_cluster=True):
        """
        :param is_redis_cluster: True表示缺失redis-py-cluster包
        """
        self._is_redis_cluster = is_redis_cluster

    def __str__(self):
        return "Need to install %s package" % ("redis-py-cluster" if self._is_redis_cluster else "redis")


class RedisToolFactory(object):
    """
    获取RedisToll实例
    """
    redis_host = None
    redis_port = None
    is_redis_cluster = True
    __redis_tool = None

    @staticmethod
    def get_redis_tool():
        if not RedisToolFactory.__redis_tool:
            RedisToolFactory.__redis_tool = RedisTool(RedisToolFactory.redis_host, RedisToolFactory.redis_port, RedisToolFactory.is_redis_cluster)
        return RedisToolFactory.__redis_tool


class RedisTool(object):
    """创建redis应用实例.

    创建redis实例，无区别对待单机redis或者集群redis.
    提供的调用接口与redis操作有关的均为同步阻塞型调用.
    可通过gevent.monkey.patch_socket()方式来充分调度cpu.
    目前暂不支持密码操作，redis集群暂时无法采用密码模式
    目前暂不支持事务操作，redis集群暂时无法采用事务模式

    Attributes:
    _redis_instance:redis实例
    """
    def __init__(self, host, port, is_redis_cluster=True):
        """
        初始化创建redis实例
        :param host:(string)地址
        :param port:(int)端口
        :param is_redis_cluster:(boolean)默认获取redis集群实例
        :return: redis实例
        """
        # 单机redis连接池
        self.__redis_pool = None
        # redis订阅操作
        self.__redis_subscribe = None
        try:
            if is_redis_cluster:
                startup_nodes = [{"host": host, "port": port}]
                self._redis_instance = rediscluster.StrictRedisCluster(startup_nodes=startup_nodes, decode_responses=True, max_connections=500)
                # 集群模式下需要先调用下某个方法，让客户端感知到服务端的所有集群ip和地址
                self._redis_instance.ping()
            else:
                self.__redis_pool = redis.ConnectionPool(host=host, port=port, db=0)
                self._redis_instance = redis.Redis(connection_pool=self.__redis_pool)
            self.__redis_subscribe = self._redis_instance.pubsub()
        except NameError:
            raise RedisEnvironmentException(is_redis_cluster)

    def close(self):
        """
        关闭相关连接.
        redis具体实例目前未发现api有暴露关闭接口
        """
        if self.__redis_pool is not None:
            self.__redis_pool.disconnect()

    def set(self, key, value, ex=None):
        """
        设置key和value值
        :param key:键
        :param value:值
        :param ex:过期时间，单位s
        """
        self._redis_instance.set(key, value, ex=ex)

    def get(self, key):
        """
        通过键获取值
        :param key:键
        :return:值，当key不存在时候返回None
        """
        return self._redis_instance.get(key)

    def delete(self, *keys):
        """
        删除redis中的指定key
        :param keys:多个键
        :return:成功删除key的数量
        """
        return self._redis_instance.delete(*keys)

    def exists(self, key):
        """
        判断key是否已经存在redis中
        :param key:键
        :return:（boolean）True存在
        """
        return self._redis_instance.exists(key)

    def expire(self, key, seconds):
        """
        设置redis中key的过期时间
        :param key:键
        :param seconds:(int)秒
        :return:设置成功返回1，key不存在或者当前不支持设置过期时间均返回0
        """
        return self._redis_instance.expire(key, seconds)

    def push(self, list_key, value, top_tail_flag=True):
        """
        将对应的值存入redis的list中
        :param list_key:list对应的key
        :param value:值
        :param top_tail_flag:（boolean）True存入队列头部，False存入尾部
        """
        if top_tail_flag:
            self._redis_instance.lpush(list_key, value)
        else:
            self._redis_instance.rpush(list_key, value)

    def pop(self, list_key, top_tail_flag=True):
        """
        获取对应list列表的第一个或者最后一个值
        :param list_key:list对应的key
        :param top_tail_flag:（boolean）True从队列头部取值，False从尾部取值
        :return:值，list_key不存在时候返回None
        """
        if top_tail_flag:
            return self._redis_instance.lpop(list_key)
        else:
            return self._redis_instance.rpop(list_key)

    def list_index(self, list_key, index):
        """
        返回列表 key 中，下标为 index 的元素。
        :param list_key:list对应的key
        :param index:索引，负数则从负方向开始搜索
        :return:值，list_key不存在时候返回None
        """
        return self._redis_instance.lindex(list_key, index)

    def block_pop(self, list_key, top_tail_flag=True, timeout=0):
        """
        获取对应list列表的第一个或者最后一个值。
        pop方法的阻塞调用，当list_key不存在时候会阻塞，直到获取一个值或者超时才返回
        :param list_key:list对应的key
        :param top_tail_flag:（boolean）True从队列头部取值，False从尾部取值
        :param timeout:(int) 秒，表示等待超时时间，0表示永久等待
        :return:值
        """
        if top_tail_flag:
            return self._redis_instance.blpop(list_key, timeout)[1]
        else:
            return self._redis_instance.brpop(list_key, timeout)[1]

    def get_list_length(self, list_key):
        """
        获取list列表长度
        :param list_key:list对应的key
        :return:长度（int）
        """
        return self._redis_instance.llen(list_key)

    def get_list_range(self, key, start, end):
        """
        获取list的区间数据,注意注意包含end那一位,如(0,3) 返回的是4个元素
        :param key:键
        :param start:起始位置
        :param end:终止位置
        :return:[]
        """
        return self._redis_instance.lrange(key, start, end)

    def list_trim(self, key, start, end):
        """
        对一个列表进行修剪(trim)，就是说，让列表只保留指定区间内的元素，不在指定区间之内的元素都将被删除
        :param key:键
        :param start:起始位置
        :param end:终止位置
        """
        self._redis_instance.ltrim(key, start, end)

    def subscribe(self, callback, *keys):
        """
        监听一系列key发布消息
        :param callback:(func(key,value))订阅通知回调
        :param keys:多个键
        """
        self.__redis_subscribe.subscribe(*keys)
        for item in self.__redis_subscribe.listen():
            if item['type'] == 'message':
                callback(item['channel'], item['data'])

    def un_subscribe(self, *keys):
        """
        取消一系列key订阅
        :param keys:多个键
        """
        self.__redis_subscribe.unsubscribe(*keys)

    def publish(self, key, value):
        """
        发布消消
        :param key:键
        :param value:值
        :return:(int)接收到消息的订阅者数量
        """
        return self._redis_instance.publish(key, value)

    def fuzzy_subscribe(self, callback, *keys):
        """
        模糊监听一系列key发布消息
        :param callback:(func(key,value))订阅通知回调
        :param keys:多个键
        """
        self.__redis_subscribe.psubscribe(*keys)
        for item in self.__redis_subscribe.listen():
            if item['type'] == 'pmessage':
                callback(item['channel'], item['data'])

    def un_fuzzy_subscribe(self, *keys):
        """
        取消一系列key模糊订阅
        :param keys:多个键
        """
        self.__redis_subscribe.punsubscribe(*keys)

    def get_pipeline(self):
        """
        获取redis执行管道，允许批量执行命令
        批量执行之后，通过execute()执行操作并返回结果
        redis集群默认不支持事务模式
        :return:Pipeline
        """
        return self._redis_instance.pipeline(transaction=False)

    def hash_get(self, key, field):
        """
        获取redis中hash结构key所对应的field值
        :param key:hash键
        :param field:hash字段
        :return:值，不存在返回None
        """
        return self._redis_instance.hget(key, field)

    def hash_set(self, key, field, value):
        """
        设置redis中hash结构key所对应的field值
        :param key:hash键
        :param field:hash字段
        :param value:值
        """
        self._redis_instance.hset(key, field, value)

    def hash_map_set(self, key, mapping):
        """
        批量设置redis中hash结构key所对应的键值对
        :param key:hash键
        :param mapping:{}结构
        """
        self._redis_instance.hmset(key, mapping)

    def hash_map_get(self, key, fields, *args):
        """
        批量获取redis中hash结构key所对应的值
        :param key:hash键
        :param fields:[]获取字段集合
        :param args:
        :return: values:[]
        """
        return self._redis_instance.hmget(key, fields, *args)

    def hash_exists(self, key, field):
        """
        返回redis的hash结构中key是否存在field域值
        :param key:hash键
        :param field:hash字段
        :return:（boolean）True存在
        """
        return self._redis_instance.hexists(key, field)

    def hash_keys(self, key):
        """
        返回redis的hash结构中key所有的field域
        :param key:hash键
        :return:[]
        """
        return self._redis_instance.hkeys(key)

    def hash_values(self, key):
        """
        返回redis的hash结构中key所有的域值
        :param key:hash键
        :return:[]
        """
        return self._redis_instance.hvals(key)

    def set_add(self, key, *values):
        """
        设置redis中set结构key对应的值
        :param key:键
        :param values:值
        """
        self._redis_instance.sadd(key, *values)

    def set_members(self, key):
        """
        返回redis中set结构key对应的值
        :param key:键
        :return: []
        """
        return self._redis_instance.smembers(key)

    def set_remove(self, key, *members):
        """
        从redis的set结构中移除key对应的members
        :param key:键
        :param member:多个value
        :return:移除数量
        """
        return self._redis_instance.srem(key, *members)

    def block_rpop_lpush(self, source, destination, timeout=0):
        """
        阻塞等待从source尾部弹出元素插入到destination头部
        :param source:源
        :param destination:目的
        :param timeout:超时时间，默认0表示一直阻塞
        :return:[,] 数组第一值为弹出元素，第二值为等待时常
        """
        return self._redis_instance.brpoplpush(source, destination, timeout)

    def increase(self, key):
        """
        逐1增加计数器
        :param key:
        :return:当前累加值
        """
        return self._redis_instance.incr(key)

    def flush_all(self):
        self._redis_instance.flushall()

if __name__ == "__main__":
    # 具体使用api可参照如下例子
    def print_result(key, value):
        print("%s:%s" % (key, value))
    import time
    redis_tool = RedisTool("10.10.1.29", 6379, False)

    redis_tool.set("ni", "ni")
    print redis_tool.get("ni")
    redis_tool.expire("ni", 1)
    time.sleep(1)
    print redis_tool.exists("ni")
    redis_tool.set("ni", "ni")
    redis_tool.delete("ni")
    print redis_tool.exists("ni")

    redis_tool.push("list1", "aa")
    redis_tool.push("list1", "bb", False)
    print redis_tool.pop("list1")
    print redis_tool.block_pop("list1", False, 2)

    print redis_tool.get_list_length("list1")

    list_range = range(0, 1000)
    pipeline = redis_tool.get_pipeline()
    for i in list_range:
        pipeline.set("%s" % i, i)
    pipeline.execute()
    for i in list_range:
        pipeline.get("%s" % i)
    print pipeline.execute()

    redis_tool.hash_set("zmm", "a1", "bbb")
    redis_tool.hash_set("zmm", "a2", "cc")
    print redis_tool.hash_get("zmm", "a1")
    print redis_tool.hash_exists("zmm", "a1")
    print redis_tool.hash_keys("zmm")
    print redis_tool.hash_values("zmm")
    redis_tool.fuzzy_subscribe(print_result, 'foo', 'bar')
    redis_tool.close()

