#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.thread.handler_thread import Runnable, Timer, HandlerThread
from common.log.manager import LogManager
from redis_tool import RedisTool
logger = LogManager.register_log("redis_handle_thread")


class CommitRunnable(object):
    """进行redis pipeline的提交"""
    pass


class SAddRunnable(object):
    __slots__ = ("func", "args", "name")

    def __init__(self, func, name, *args):
        self.func = func
        self.args = args
        self.name = name

    def run(self):
        self.func(self.name, *self.args)


class RedisPipeHandlerThread(HandlerThread):
    """Redis批量操作协程

    提供redis的批量操作，外部对pipe_line进行操作，内部默认seconds执行一次提交.
    同时若操作量超过__max_operates也执行一次提交
    该模块外部使用时候必须配合gevent框架使用
    """
    def __init__(self, seconds, host, port, is_redis_cluster=True):
        super(RedisPipeHandlerThread, self).__init__()
        self.__timer = Timer(seconds)
        self.__redis_helper = RedisTool(host, port, is_redis_cluster)
        self.__pipe_line = self.__redis_helper.get_pipeline()
        self.__max_operates = 10000

    def _run(self):
        while True:
            runnable = self._internal_queue.get()
            if runnable is None:
                self.__running = False
                break
            try:
                if isinstance(runnable, CommitRunnable):
                    if len(self.__pipe_line.command_stack) == 0:
                        continue
                    result = self.__pipe_line.execute()
                    logger.info("Redis Pipeline execute by timer.count:%s" % len(result))
                else:
                    runnable.run()
                if len(self.__pipe_line.command_stack) > self.__max_operates:
                    result = self.__pipe_line.execute()
                    logger.info("Redis Pipeline execute by size.count:%s" % len(result))
            except Exception, e:
                logger.exception("%s:_run:UnKnown Error.Must be careful.Error:%s" % (self.__class__.__name__, e))
                break
        self.__redis_helper.close()

    def start(self):
        super(RedisPipeHandlerThread, self).start()
        self.__timer.post(Runnable(self.commit))

    def stop(self):
        super(RedisPipeHandlerThread, self).stop()
        self.__redis_helper.close()

    def commit(self):
        self.post(CommitRunnable())
        self.__timer.post(Runnable(self.commit))

    # 以下是针对redis操作的包装
    def hash_set(self, name, key, value):
        self.post(Runnable(self.__pipe_line.hset, name, key, value))

    def set_add(self, name, *values):
        self.post(SAddRunnable(self.__pipe_line.sadd, name, *values))

    def hash_map_set(self, name, mapping):
        self.post(Runnable(self.__pipe_line.hmset, name, mapping))

    def flush_all(self):
        self.__redis_helper.flush_all()





