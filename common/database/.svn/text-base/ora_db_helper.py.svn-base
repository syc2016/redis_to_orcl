#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.log.manager import LogManager
logger = LogManager.register_log("ora_db_helper")

# 系统环境中是否支持连接Oracle数据库
Oracle_DB_Flag = True

try:
    import cx_Oracle
    # 需要设置如下，才能对查询和结果中的中文进行识别
    import os
    os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'
except Exception, e:
    Oracle_DB_Flag = False


class OracleConnectedException(Exception):
    def __init__(self, origin_error):
        self.origin_error = origin_error
        super(OracleConnectedException, self).__init__()

    def __str__(self):
        return "Unable to Connected to Oracle,error:%s" % self.origin_error


class OraDbHelper(object):
    """
    连接操作Oracle数据库
    """
    def __init__(self):
        # 连接对象
        self.conn = None
        # 数据库游标
        self.cursor = None
        # 用户名
        self.ora_user = None
        # 密码
        self.ora_pwd = None
        # ip
        self.ora_ip = None
        # 端口
        self.ora_port = None
        # 实例
        self.ora_sid = None

    def is_alive(self):
        """
        是否连接
        :return:True or False
        """
        keep_alive_sql = "select 1 from dual"
        try:
            self.cursor.execute(keep_alive_sql)
            alive_flag = True
        except:
            alive_flag = False
        return alive_flag

    def clone(self):
        """
        创建一个新的oracle连接实例
        :return:OraDbHelper
        """
        ora_db_helper = OraDbHelper()
        ora_db_helper.connect(self.ora_user, self.ora_pwd, self.ora_ip, self.ora_port, self.ora_sid)
        return ora_db_helper

    def connect(self, user, pwd, ip, port, sid):
        """
        连接数
        :param user: 用户名
        :param pwd: 密码
        :param ip: ip
        :param port: 端口
        :param sid: oracle实例名
        """
        connect_str = "{0}/{1}@{2}:{3}/{4}".format(user, pwd, ip, port, sid)
        try:
            self.conn = cx_Oracle.connect(connect_str)
            self.cursor = self.conn.cursor()
            # self.is_connected = True
            self.ora_user = user
            self.ora_pwd = pwd
            self.ora_ip = ip
            self.ora_port = port
            self.ora_sid = sid
        except Exception as ex:
            logger.error("%s:Connect Oracle Failed!error:%s" % (self.__class__.__name__, ex))
            raise OracleConnectedException(ex)

    def __reconnect(self):
        """
        重新连接
        """
        logger.info("reconnect to oracle...")
        self.connect(self.ora_user, self.ora_pwd, self.ora_ip, self.ora_port, self.ora_sid)

    def execute(self, sql, params):
        """
        执行sql语句
        :param sql:sql语句
        :param params:参数字典{param1:value1,param2:value2}
        """
        try:
            self.cursor.execute(sql, params)
        except Exception as ex:
            if not self.is_alive():
                self.__reconnect()
            logger.error("%s:Insert Oracle Failed!error:%s" % (self.__class__.__name__, ex))
            raise ex

    def query(self, sql, params):
        """
        查询数据库
        :param sql:sql语句
        :param params:params:参数字典{param1:value1,param2:value2}
        :return cursor:oracle游标，外部通过cursor.fetchone()获取一条记录
        """
        try:
            self.cursor.execute(sql, params)
            return self.cursor
        except Exception as ex:
            if not self.is_alive():
                self.__reconnect()
            logger.error("%s:query Oracle Failed!error:%s" % (self.__class__.__name__, ex))
            raise ex

    def query_rows(self, sql, params):
        """
        查询数据库
        :param sql:sql语句
        :param params:params:参数字典{param1:value1,param2:value2}
        :return 返回行集合
        """
        try:
            self.cursor.execute(sql, params)
            result = self.cursor.fetchall()
            return result
        except Exception as ex:
            if not self.is_alive():
                self.__reconnect()
            logger.error("%s:query Oracle Failed!error:%s" % (self.__class__.__name__, ex))
            raise ex

    def query_count(self, sql, params):
        """
        查询记录数
        """
        try:
            self.cursor.execute(sql, params)
            result = self.cursor.fetchone()
            return result[0]
        except Exception as ex:
            if not self.is_alive():
                self.__reconnect()
            logger.error("%s:query Oracle Failed!error:%s" % (self.__class__.__name__, ex))
            raise ex

    def start_transaction(self):
        """
        开启事务
        """
        try:
            self.conn.begin()
        except Exception as ex:
            if not self.is_alive():
                self.__reconnect()
            logger.error("%s:Start Oracle Transaction Failed!error:%s" % (self.__class__.__name__, ex))
            raise ex

    def commit(self):
        """
        先调用start_transaction开启事务
        提交事务
        """
        try:
            self.conn.commit()
        except Exception as ex:
            self.conn.rollback()
            logger.error("%s:commit Oracle Failed!error:%s" % (self.__class__.__name__, ex))
            if not self.is_alive():
                self.__reconnect()

    def rollback(self):
        try:
            self.conn.rollback()
        except Exception as ex:
            if not self.is_alive():
                self.__reconnect()
            logger.error("%s:rollback Oracle Failed!error:%s" % (self.__class__.__name__, ex))
            raise ex

    def close(self):
        """
        关闭连接
        """
        try:
            self.cursor.close()
            self.conn.close()
        except Exception as ex:
            logger.error("%s:Close Oracle Failed!error:%s" % (self.__class__.__name__, ex))