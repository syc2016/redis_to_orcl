#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.database.db_helper import DBHelper
from common.thread.handler_thread import Runnable, Timer
from common.thread.origin_handler_thread import OriginHandlerThread


class SaveRunnable(Runnable):
    """进行sql的执行"""
    pass


class CommitRunnalbe(Runnable):
    """进行sql的提交"""
    pass


class DBHandlerThread(OriginHandlerThread):
    """
    包装了DBHelper的线程级别HandlerThread
    """
    def __init__(self, path):
        super(DBHandlerThread, self).__init__()
        self.path = path
        self._db_helper = None
        self._timer = Timer(1)
        # 线程退出标志
        self.thread_exit_flag = True

    def _run(self):
        """
        由于数据库连接对象必须操作在同一个线程内部
        所以将DBHelper实例化在该方法中
        """
        self._db_helper = DBHelper()
        self._db_helper.connect(self.path)
        # 执行了插入数据库的数量
        save_count = 0
        while True:
            runnable = self._internal_queue.get()
            # runnable为None，用于表示退出操作。这时，执行循环结束，当前的协程结束，无法继续使用
            if runnable is None:
                self.__running = False
                break
            if isinstance(runnable, SaveRunnable):
                save_count += 1
                runnable.run()
            if isinstance(runnable, CommitRunnalbe):
                if save_count == 0:
                    continue
                else:
                    runnable.run()
                    save_count = 0
        self._db_helper.close()
        self.thread_exit_flag = True

    def start(self):
        super(DBHandlerThread, self).start()
        self._timer.post(Runnable(self.commit))
        self.thread_exit_flag = False

    def save(self, sql, params):
        """
        执行sql语句
        :param sql:sql语句
        :param params:参数元组(param1,param2)
        """
        self.post(SaveRunnable(self._db_helper.save, sql, params))

    def commit(self):
        """
        提交事务
        """
        if self._completed:
            return
        else:
            self.post(CommitRunnalbe(self._db_helper.commit))
            self._timer.reset()
            self._timer.post(Runnable(self.commit))





