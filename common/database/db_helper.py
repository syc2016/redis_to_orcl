#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
import os
import sqlite3
from common.log.manager import LogManager
logger = LogManager.register_log("db_helper")


class DBHelper(object):
    """
    操作Sqlite3数据库
    数据库连接对象必须操作在同一个线程内
    """
    def __init__(self):
        # 连接对象
        self.conn = None
        # 数据库游标
        self.cursor = None
        # 与数据库连接状态
        self.is_connected = False

    def is_connected(self):
        """
        是否连接
        :return:True or False
        """
        return self.is_connected

    def connect(self, path):
        """
        连接数据库
        :param path: 数据库路径
        """
        if os.path.exists(path) and os.path.isfile(path):
            try:
                self.conn = sqlite3.connect(path)
                self.cursor = self.conn.cursor()
                self.is_connected = True
            except sqlite3.Error, e:
                logger.error("%s:Connect DB Failed!Path:%s,error:%s" % (self.__class__.__name__, path, e))
        else:
            raise AssertionError("%s:The DB File is not exist!Path:%s" % (self.__class__.__name__, path))

    def save(self, sql, params):
        """
        执行sql语句
        :param sql:sql语句
        :param params:参数元组(param1,param2)
        """
        if not self.is_connected:
            logger.error("%s:Insert DB Failed!DB is not connected!" % self.__class__.__name__)
            return
        try:
            # logger.debug("%s:insert sql into DB" % self.__class__.__name__)
            self.cursor.execute(sql, params)
        except sqlite3.Error, e:
            logger.error("%s:Insert DB Failed!error:%s" % (self.__class__.__name__, e))

    def query(self, sql, params, func):
        """
        查询数据库
        :param sql:sql语句
        :param params:参数元组(param1,param2)
        :param func:查询到数据行后的回调
        """
        if not self.is_connected:
            logger.error("%s:query DB Failed!DB is not connected!" % self.__class__.__name__)
            return
        try:
            self.cursor.execute(sql, params)
            for row in self.cursor:
                func(row)
        except sqlite3.Error, e:
            logger.error("%s:query DB Failed!error:%s" % (self.__class__.__name__, e))

    def query_rows(self, sql, params):
        """
        查询数据库
        :param sql:sql语句
        :param params:参数元组(param1,param2)
        :return 返回行集合
        """
        if not self.is_connected:
            logger.error("%s:query DB Failed!DB is not connected!" % self.__class__.__name__)
            return
        try:
            self.cursor.execute(sql, params)
            result = []
            for row in self.cursor:
                result.append(row)
            return result
        except sqlite3.Error, e:
            logger.error("%s:query DB Failed!error:%s" % (self.__class__.__name__, e))

    def query_count(self, sql, params):
        """
        查询记录数
        """
        if not self.is_connected:
            logger.error("%s:query DB Failed!DB is not connected!" % self.__class__.__name__)
            return
        try:
            self.cursor.execute(sql, params)
            for row in self.cursor:
                return row[0]
        except sqlite3.Error, e:
            logger.error("%s:query DB Failed!error:%s" % (self.__class__.__name__, e))

    def commit(self):
        """
        提交事务
        """
        if not self.is_connected:
            logger.error("%s:commit DB Failed!DB is not connected!" % self.__class__.__name__)
            return
        try:
            # logger.debug("%s:commit sql into DB" % self.__class__.__name__)
            self.conn.commit()
        except sqlite3.Error, e:
            self.conn.rollback()
            logger.error("%s:commit DB Failed!error:%s" % (self.__class__.__name__, e))

    def close(self):
        """
        关闭连接
        """
        if self.is_connected:
            try:
                self.cursor.close()
                self.conn.close()
                self.is_connected = False
            except sqlite3.Error, e:
                logger.error("%s:Close DB Failed!error:%s" % (self.__class__.__name__, e))
