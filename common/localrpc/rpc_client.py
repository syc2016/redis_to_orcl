#!/usr/bin/env python
# coding: utf-8

__author__ = 'zmmgiser'
import pickle
import struct
import uuid
import gevent
from gevent.event import Event

from common.transport.client_session_base import ClientSessionBase
from rpc_parcel import RpcParcel
from rpc_socket_client import RpcSocketClient
from common.log.manager import LogManager
from common.transport.config import TransportConfig
from config import LocalRpcConfig
logger = LogManager.register_log("rpc_client")


class RpcResultHolder(object):
    """
    调用代理方法缓存数据
    """
    def __init__(self):
        # 调用代理方法等待锁集合.key:request_id，value:gevnet.Event()
        self.__waitEventList = {}
        # 调用结果返回列表
        self.__resultList = {}

    def wait(self, request_id):
        """
        等待结果
        :param request_id:
        :return: 调用结果
        """
        wait_event = Event()
        self.__waitEventList[request_id] = wait_event
        wait_event.wait()
        if request_id not in self.__resultList:
            # 可能由于非正常原因，比如断开与服务端的连接
            return None
        else:
            result = self.__resultList.pop(request_id)
            return result

    def set_result(self, request_id, result):
        """
        设置结果
        :param request_id:
        :param result: 返回结果
        """
        if request_id in self.__waitEventList:
            self.__resultList[request_id] = result
            self.__notify(request_id)
        else:
            logger.error("%s:set_result request_id is not in __waitEventList.request_id:%s" % (self.__class__.__name__, uuid.UUID(bytes=request_id)))

    def __notify(self, request_id):
        """
        唤醒调用锁
        :param request_id:
        :return:
        """
        wait_event = self.__waitEventList.pop(request_id)
        wait_event.set()

    def notify_all(self):
        """
        唤醒所有锁，可能由于与服务端断开连接，这个是时候需要调用，不能死锁在这里
        """
        for key in self.__waitEventList:
            wait_event = self.__waitEventList[key]
            wait_event.set()
        self.__waitEventList.clear()


class RpcClient(ClientSessionBase):
    """
    本地进程间通讯客户端
    子类继承后需要实现代理的rpc方法
    """
    def __init__(self, ip, port, func_online=None):
        super(RpcClient, self).__init__(ip, port, self.__cb_rpc_online, False)
        self.rpc_result_holder = RpcResultHolder()
        # 存储RPC回调字典。key:LocalRpcType,value:callback回调函数
        self.callback_dict = {}
        self.func_online = func_online
        self.request_id = 0

    def set_socket_client(self, ip, port):
        self._socket_client = RpcSocketClient(ip, port, self._create_parcel(), self._cb_write, self._cb_read, self._cb_offline)

    def __cb_rpc_online(self, ret_code=TransportConfig.NO_ERROR):
        if self.func_online is not None:
            self.func_online(ret_code)
        if ret_code != TransportConfig.NO_ERROR:
            # 断开连接，唤醒所有等待锁
            self.rpc_result_holder.notify_all()

    def _cb_read(self, data, system_region, func_id, request_id):
        if data["error"] is None:
            pass
        else:
            logger.error("%s:Local Rpc Client receive response but a error:%s.system_region:%s,msg_type:%s" %
                         (self.__class__.__name__, data["error"], system_region, func_id))
        result = pickle.loads(data["result"])
        if self._cb_read_rpc(func_id, result):
            self.callback_dict[func_id](result)
        else:
            self.rpc_result_holder.set_result(request_id, result)

    def _create_parcel(self):
        return RpcParcel()

    def send_data(self, system_region, msg_type, request_id, content):
        """
        主动调用发送数据，并通知发送IO就绪
        :param system_region: 系统区号
        :param msg_type: 包子类型
        :param request_id: 请求ID
        :param content: 包体
        """
        while not self._socket_client.get_alive():
            logger.warning("%s:The local server is offline.wait 1s and try again" % self.__class__.__name__)
            gevent.sleep(1)
        if content is not None:
            temp_content_length = len(content)
            data_buf = struct.pack("!IHHQ%ss" % temp_content_length, temp_content_length,
                                   system_region, msg_type, request_id, content)
        else:
            data_buf = struct.pack("!IHHQ", 0, system_region, msg_type, request_id)
        self._socket_client.post_data(data_buf)
        self._socket_client.notify_write()

    def _do_rpc_function(self, func_id, params):
        """
        执行具体的Rpc方法，并等待返回值
        :param func_id:功能id
        :param params:参数
        """
        request_id = self.request_id
        self.request_id += 1
        self.send_data(LocalRpcConfig.SYSTEM_ID, func_id, request_id, params)
        result = self.rpc_result_holder.wait(request_id)
        return result

    def _cb_read_rpc(self, func_id, result):
        """
        RPC回调消息处理,需要交由子类处理
        :return:结果必须返回True或者False
        """
        return False

