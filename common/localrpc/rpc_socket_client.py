#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.transport.socket_client import SocketClient
from common.localrpc.rpc_message import RpcMessage


class RpcSocketClient(SocketClient):
    def set_message(self, parcel):
        self._Message = RpcMessage(parcel)
