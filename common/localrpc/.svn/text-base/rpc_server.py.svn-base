#!/usr/bin/env python
# coding: utf-8

__author__ = 'zmmgiser'
import pickle
import struct
from common.transport.socket_server import SocketServer, SocketSession
from common.utils import Utils
from common.localrpc.rpc_parcel import RpcParcel
from common.localrpc.rpc_message import RpcMessage
from common.log.manager import LogManager
logger = LogManager.register_log("rpc_server")


class RpcServer(SocketServer):
    """
    本地进程间通讯服务端
    """
    def __init__(self, port):
        super(RpcServer, self).__init__(port)

    def _create_parcel(self):
        return RpcParcel()

    def _create_session(self, socket_session, address):
        rpc_session = RpcSession(socket_session, address, self._create_parcel(), None, self._read_handle)
        return rpc_session

    def _read_handle(self, socket_session, data, system_region, func_id, request_id):
        if data is not None:
            params = data["params"]
            result = self._do_rpc_function(func_id, params)
        else:
            result = self._do_rpc_function(func_id, None)
        socket_session.send_data(system_region, func_id, request_id, result)

    def start(self):
        self._server.serve_forever()

    def _do_rpc_function(self, func_id, params):
        """
        具体执行Rpc过程，需要由子类继承
        :param params: 调用参数
        :return: rpc调用结果
        """
        return AssertionError('%s: _do_rpc_function must be implemente!' % self.__class__.__name__)


class RpcSession(SocketSession):
    def __init__(self, socket_session, address, parcel, func_write, func_read):
        super(RpcSession, self).__init__(socket_session, address, parcel, func_write, func_read, self._handle_offline, False)

    def set_message(self, parcel):
        self._Message = RpcMessage(parcel)

    def _handle_offline(self):
        """
        断开连接回调
        """
        logger.info("Rpc server session:%s:%s is offline" % self._address)

    def send_data(self, system_region, msg_type, request_id, content):
        """
        主动调用发送数据
        :param system_region: 系统区号
        :param msg_type: 包子类型
        :param request_id: 请求ID
        :param content: 包体
        """
        try:
            bytes = pickle.dumps(content)
            bytes_str = Utils.get_resultJsonStr(bytes)
            temp_content_length = len(bytes_str)
            data_buf = struct.pack("!IHHQ%ss" % temp_content_length,temp_content_length,
                                   system_region, msg_type, request_id, bytes_str)
            self.post_data(data_buf)
        except Exception, e:
            logger.exception("%s:send_data,msg_type:%s,content:%s,error:%s" % (self.__class__.__name__, hex(msg_type), content, e))