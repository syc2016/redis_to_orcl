#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.transport.parcel_tool import Message


class RpcMessage(Message):
    def set_buf_format_type(self):
        self._head_format_type = "!IHHQ"
        self._buf_format_type = "!IHHQ%ss"
        self._head_size_type = "!16s"
        self._head_buf_size = 16