# coding=utf-8
__author__ = 'wangshuo'


class StateMachine(object):
    """状态机类型。状态机有一个起始状态和一个结束状态。
    这是一个同步的状态机。状态机不具备执行性，通过被调用者阻塞调度。"""

    def __init__(self, root, init_state, terminate_state):
        """初始化"""
        self.root = root
        self.__init_state = init_state
        self.__terminate_state = terminate_state
        # 启动标志
        self.__started = False
        # 当前状态
        self.__cur_state = None
        # defer队列
        self.__defer_event = None

    def start(self):
        """启动状态机。状态机只能启动一次。同步函数。"""
        if self.__started:
            raise Exception('StateMachine already started!')
        self.__started = True
        self.__cur_state = self.__init_state
        self.__cur_state._on_enter(self)

    def process_event(self, event):
        """处理事件，把Event传递给当前的状态cur_state。同步函数。
        返回True / False表示事件处理与否"""
        # assert isinstance(event, Event), 'input not Event'
        # if not self.__started:
        #     raise Exception('StateMachine not started!')
        # if self.__cur_state is None:
        #     raise Exception('StateMachine has no state!')
        return self.__cur_state._process_event(self, event)

    def _translate(self, state):
        """转换状态，只供State内部使用。"""
        # 退出源状态
        if self.__cur_state is not None:
            self.__cur_state._on_exit(self)
        # 实例化目标状态
        self.__cur_state = state
        # 进入目标状态
        self.__cur_state._on_enter(self)
        # defer处理
        if self.__defer_event:
            temp_event = self.__defer_event
            self.__defer_event = None
            self.__cur_state._process_event(self, temp_event)

    def _defer(self, event):
        """延迟事件入队列，只供State内部使用。"""
        self.__defer_event = event

    def get_current_state(self):
        return self.__cur_state

    def __str__(self):
        return 'Started: %s, Root: %s, InitState: %s, TerminateState: %s, CurState: %s, DeferEvent: %s' % \
               (self.__started, self.root, self.__init_state, self.__terminate_state, self.__cur_state,
                self.__defer_event)


class State(object):
    """状态类。状态机由不同的状态组成。通过建立reactions转换表实现状态的逻辑处理。
    转换表结构体：dict：key[Event Class], value[Action / Translation]"""

    def __init__(self):
        # 转换表
        self.reactions = {}

    def _on_enter(self, state_machine):
        """进入触发。供子类override"""
        pass

    def _on_exit(self, state_machine):
        """离开触发。供子类override"""
        pass

    def defer_event(self, state_machine, event):
        """延迟推送事件。所谓延迟推送，是在状态机的状态切换的时候的传递给下一个状态的消息触发队列。"""
        if not isinstance(event, Event):
            raise TypeError('%s : defer_event : [%s] should be a Event!' % (self.__class__.__name__, event))
        state_machine._defer(event)

    def _process_event(self, state_machine, event):
        """内部处理事件，只供StateMachine内部使用。"""
        if event is None:
            return False
        event_class = type(event)
        if event_class not in self.reactions:
            return False
        action = self.reactions[event_class]
        if action is None:
            return False
        if isinstance(action, Translation):
            action._translate(state_machine)
        elif isinstance(action, Action):
            translation = action._call(state_machine, event)
            if translation is not None:
                translation._translate(state_machine)
        return True

    def __str__(self):
        return '%s' % type(self)


class Action(object):
    """操作类。所谓操作，就是状态逻辑处理的操作。内部通过封装函数引用实现。
    操作在State的reactions中使用。"""

    def __init__(self, func):
        self.__func = func

    def _call(self, state_machine, event):
        """调用操作。供State内部使用。"""
        return self.__func(state_machine, event)


class Translation(object):
    """转换类。所谓转换，就是从一个State转换到另外一个状态。"""

    def __init__(self, state):
        self.__state = state

    def _translate(self, state_machine):
        """转换操作。供StateMachine内部使用。"""
        state_machine._translate(self.__state)


class Event(object):
    """事件类。事件是State进行操作的触发。"""
    pass


class Queue(object):
    def __init__(self):
        self.queue = []

    def enqueue(self, item):
        self.queue.append(item)

    def dequeue(self):
        if self.queue != []:
            return self.queue.pop(0)
        else:
            return None

    def head(self):
        if self.queue != []:
            return self.queue[0]
        else:
            return None

    def tail(self):
        if self.queue != []:
            return self.queue[-1]
        else:
            return None

    def length(self):
        return len(self.queue)

    def empty(self):
        return self.queue == []

    def __str__(self):
        return '%s', self.queue
