#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from functools import wraps


def singleton(cls):
    """ 单例模式，使用类装饰器实现

    :param cls: 类名
    """
    instance = {}

    @wraps(cls)
    def _singleton(*args, **kw):
        if cls not in instance:
            instance[cls] = cls(*args, **kw)
        return instance[cls]
    return _singleton


class Singleton(type):
    """基于__metaclass__（元类）的单利模式"""
    def __init__(cls, name, bases, dict):
        super(Singleton, cls).__init__(name, bases, dict)
        cls._instance = None

    def __call__(cls, *args, **kw):
        if cls._instance is None:
            cls._instance = super(Singleton, cls).__call__(*args, **kw)
        return cls._instance
