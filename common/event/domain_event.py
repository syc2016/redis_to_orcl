#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
import json
import datetime
import time
import string


class DomainEvent(object):
    def __init__(self):
        self.id = 0
        self.type_name = ''
        self.occurred_on = 0
        self.body = dict()

    def serialize(self):
        event_dict = dict()
        event_dict['id'] = self.id
        event_dict["typeName"] = self.type_name
        event_dict["occurredOn"] = long((time.time() * 1000) + string.atoi(datetime.datetime.now().strftime('%f')[:3]))
        event_dict["body"] = self.on_child_serialize()
        return json.dumps(event_dict)

    def deserialize(self, data):
        event_dict = json.loads(data)
        self.id = event_dict["id"]
        self.type_name = event_dict["typeName"]
        self.occurred_on = event_dict["occurredOn"]
        self.body = event_dict["body"]

    def on_child_serialize(self):
        raise NotImplementedError("on_child_serialize must be implement")

