#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.utils import Utils
from domain_event import DomainEvent


class DomainEventListener(object):
    """客户端事件监听器"""
    def __init__(self, redis_tool, domain_name, queue_name, listen_event_types):
        self.redis_tool = redis_tool
        # 事件域
        self.domain_name = domain_name
        # 服务单路由
        self.exchange_name = Utils.get_exchange_name(self.domain_name)
        # 监听队列
        self.queue_name = queue_name
        # 监听事件类型过滤集合[]
        self.listen_event_types = listen_event_types
        # 是否开始监听
        self.alive_flag = False
        # redis执行brpoplpush阻塞时间
        self.__block_timeout = 5

    def stop(self):
        """
        停止监听
        """
        self.alive_flag = False
        self.redis_tool.set_remove(self.exchange_name, self.queue_name)
        source_queue_name = Utils.get_listen_queue_name(self.domain_name, self.queue_name)
        copy_queue_name = Utils.get_copy_queue_name(source_queue_name)
        self.redis_tool.delete(source_queue_name, copy_queue_name)

    def listen(self):
        """
        开始监听
        1.判断监听路由中是否已经包含自己的监听队列，不包含则删除一下监听队列的数据（防止上次取消监听残留的不一致性数据）
        2.检查备用队列中是否有未处理消息
        3.从监听队列获取消息并存入备用队列（保证可靠性）
        4.消息消费成功，从备用队列删除消息
        """
        source_queue_name = Utils.get_listen_queue_name(self.domain_name, self.queue_name)
        copy_queue_name = Utils.get_copy_queue_name(source_queue_name)
        # 判断是否已经监听
        listener_queue_list = self.redis_tool.set_members(self.exchange_name)
        if self.queue_name not in listener_queue_list:
            self.redis_tool.delete(source_queue_name, copy_queue_name)
            # 向路由注册监听队列
            self.redis_tool.set_add(self.exchange_name, self.queue_name)
        self.alive_flag = True
        # 先从备用队列中判断是否有未消费消息
        copy_msg_id = None
        copy_queue_length = self.redis_tool.get_list_length(copy_queue_name)
        if copy_queue_length != 0:
            copy_msg_id = self.redis_tool.list_index(copy_queue_name, 0)
        while True and self.alive_flag:
            if copy_msg_id is not None:
                msg_id = copy_msg_id
            else:
                value = self.redis_tool.block_rpop_lpush(source_queue_name, copy_queue_name, self.__block_timeout)
                msg_id = value
            result = False
            while not result and msg_id is not None:
                data = self.redis_tool.get(Utils.get_msg_key(self.domain_name, msg_id))
                event = DomainEvent()
                event.deserialize(data)
                if event.type_name in self.listen_event_types:
                    result = self.filter_dispatch(event)
                else:
                    result = True
                if result:
                    self.redis_tool.pop(copy_queue_name, True)
                    break
                else:
                    msg_id = self.redis_tool.list_index(copy_queue_name, 0)
            copy_msg_id = None

    def filter_dispatch(self, event):
        """
        处理事件
        :param event:
        :return:transaction_flag:boolean True表示事务执行成功，Fasle执行失败
        """
        raise NotImplementedError("filter_dispatch must be implemented")
