#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.utils import Utils
import string


class DomainEventPublisher(object):
    """服务端事件发布器"""
    def __init__(self, redis_tool, domain):
        """
        :param domain:(string)事件域
        """
        self.redis_tool = redis_tool
        self.domain = domain
        self.__expire_timeout = 1800
        msg_id = self.redis_tool.get(Utils.get_msg_id_key(self.domain))
        self.msg_id = string.atoi(msg_id) if msg_id is not None else 0

    def publish(self, events):
        """
        发布事件
        :param events:单个事件或者[event]
        """
        exchange_name = Utils.get_exchange_name(self.domain)
        listener_list = self.redis_tool.set_members(exchange_name)
        pipe_line = self.redis_tool.get_pipeline()
        if isinstance(events, list):
            for event in events:
                self.__publish_event(event, pipe_line, listener_list)
        else:
            self.__publish_event(events, pipe_line, listener_list)
        pipe_line.execute()

    def __publish_event(self, event, pipe_line, listener_list):
        self.msg_id += 1
        data = event.serialize()
        for queue_name in listener_list:
            listen_queue_name = Utils.get_listen_queue_name(self.domain, queue_name)
            msg_id_key = Utils.get_msg_id_key(self.domain)
            msg_key = Utils.get_msg_key(self.domain, self.msg_id)

            pipe_line.set(msg_key, data, self.__expire_timeout)
            pipe_line.set(msg_id_key, self.msg_id)
            pipe_line.lpush(listen_queue_name, self.msg_id)
