﻿#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
import uuid
import json
import time
import string
import datetime
from thread.handler_thread import Runnable, Timer


class Utils(object):
    """基础工具类，提供静态方法调用"""

    @staticmethod
    def float_equal(a, b):
        return abs(a - b) < 0.0000001

    @staticmethod
    def current_time_millis():
        return long(round(time.time() * 1000))

    @staticmethod
    def millis_to_seconds(millis):
        return round(millis/1000)

    @staticmethod
    def parse_time(time_stamp, format='%Y-%m-%d %H:%M:%S'):
        t = time.localtime(time_stamp / 1000)
        return time.strftime(format, t)

    @staticmethod
    def get_new_request_id():
        """
        生成新的requestId
        :return: 16位请求id
        """
        uuid_data = uuid.uuid1()
        request_id = uuid_data.bytes
        return request_id

    @staticmethod
    def get_resultJsonStr(result, error=None):
        """
        返回结果以json形式表示
        :return:{"result":"result...","error":null}
        """
        if error is None:
            return json.dumps({"result": result, "error": None})
        else:
            return json.dumps({"result": None, "error": error})

    @staticmethod
    def get_paramsJsonStr(*paramlist):
        """
        返回以json形式表示
        :param paramlist: 参数列表
        :return:{"params": ["param1","param2"]}
        """
        params = {"params": paramlist}
        return json.dumps(params)

    @staticmethod
    def get_http_result_json(code, message, result):
        """
        返回json结果
        :param code:代码
        :param message:错误消息
        :param result:{}类型
        :return:{'code':0,'msg':'ok','data':'{}'}
        """
        http_result_dict = dict()
        http_result_dict['code'] = code
        http_result_dict['msg'] = message
        http_result_dict['data'] = result
        return json.dumps(http_result_dict)

    @staticmethod
    def compare_date(YYYYMMDD):
        """
        与当天时间比较时间
        @:param YYYYMMDD:int 日期
        @:return true 表示与今天日期相同，false不同
        """
        today_date_str = time.strftime("%Y%m%d", time.localtime())
        today_date = string.atoi(today_date_str)
        if today_date == YYYYMMDD:
            return True
        else:
            return False

    @staticmethod
    def reset_resubscribe_timer(post_func, sub_timer=None, delay_hour=0):
        """
        晚上delay_hour点遇到周六周日顺延到下周一进行socket断开重启，并重新订阅
        @:params handler_thread:Timer 用于定时调用post_func
        @:param post_func 放入定时器执行的函数
        @:param delay_hour 半夜几点开始启动函数
        """
        week_str = time.strftime("%w", time.localtime())
        week = string.atoi(week_str)
        delay_day = 1
        if week == 5:
            delay_day += 2
        elif week == 6:
            delay_day += 1
        end_date = (datetime.datetime.now() + datetime.timedelta(days=delay_day)).date()
        end_time = time.mktime(end_date.timetuple())
        wait_time = end_time - time.time() + delay_hour * 3600
        if sub_timer is None:
            sub_timer = Timer(wait_time, 0)
        else:
            sub_timer.reset(wait_time, 0)
        sub_timer.post(Runnable(post_func))

    @staticmethod
    def int64_to_double(value):
        return (value * 1.0) / 10000

    @staticmethod
    def double_to_int64(value):
        return long(round(value * 10000))

    @staticmethod
    def get_exchange_name(domain):
        return domain + ".consumer"

    @staticmethod
    def get_copy_queue_name(listen_queue_name):
        return listen_queue_name + ".copy"

    @staticmethod
    def get_listen_queue_name(domain, name):
        return domain + "." + name + "." + "msg"

    @staticmethod
    def get_msg_id_key(domain):
        return domain + ".id"

    @staticmethod
    def get_msg_key(domain, msg_id):
        return domain + ".msg" + "." + str(msg_id)
