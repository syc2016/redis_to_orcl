#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
import gevent
from gevent.queue import Queue
from gevent.server import StreamServer
import gevent.monkey
gevent.monkey.patch_socket()
import time
import uuid
import struct
import ctypes
from common.log.manager import LogManager
from config import TransportConfig
from parcel_tool import Message
from common.utils import Utils
from parcel_tool import MsgParcelException
from common.thread.handler_thread import Timer, Runnable
logger = LogManager.register_log("socket_server")


class SocketServer(object):
    """
    socket服务端基类，需要被子类继承使用
    """
    def __init__(self, port):
        """
        @:param port：服务端监听端口
        @:param Handle: 参照HandleTemplete模板传入的是类型不是实例，客户端连接回调
        """
        self._server = StreamServer(("", port), self.__connection_handler)
        # 所有客户端session集合，key:session_id，value:SocketSession
        self.sessionList = {}

    def start(self):
        """
        启动服务端
        """
        gevent.spawn(self._server.serve_forever)

    def stop(self):
        """
        停止服务端
        """
        for session_id in self.sessionList:
            socket_session = self.sessionList[session_id]
            socket_session.stop()
        self.sessionList.clear()
        self._server.close()

    def _create_parcel(self):
        """
        创建解析工具，需要由子类继承
        :return BaseParcel子类
        """
        raise AssertionError('%s: _create_parcel must be implemente!' % self.__class__.__name__)

    def _create_session(self, socket_session, address):
        """
        创建session，需要由子类继承
        @:param socket_session:socket 客户端连接socket
        @:param address:(ip,port) 客户端ip和端口
        :return SocketSession子类
        """
        raise AssertionError('%s: _create_session must be implemente!' % self.__class__.__name__)

    def __connection_handler(self, socket_session, address):
        """
        @:param socket_session:socket 客户端连接socket
        @:param address:(ip,port) 客户端ip和端口
        """
        client_session = self._create_session(socket_session, address)
        # client_session = SocketSession(socket_session, address, self._create_parcel(), None, self._read_handle)
        client_session.start()
        self.sessionList[client_session.session_id] = client_session
        logger.info("IP：%s Port:%d session_id:%s establish connection to server" % (address[0], address[1], uuid.UUID(bytes=client_session.session_id)))

    def _read_handle(self, socket_session, data, system_region, func_id, request_id):
        """
        子类必须继承该方法
        @:param socket_session: SocketSession 客户端连接会话
        @:param data: 消息包体
        @:param system_region:系统区号
        @:param func_id:包子类型
        @:param request_id:请求ID
        """
        if TransportConfig.KEEP_ALIVE_FLAG:
            if func_id == TransportConfig.HEART_REQ_FID:
                logger.debug("Receive %s:%s a HeartBeat request" % (socket_session.get_ip(),socket_session.get_port()))
                data_resp_bytes = socket_session.get_msgtool().pack_message(system_region, TransportConfig.HEART_RESP_FID, request_id, None)
                socket_session.post_data(data_resp_bytes)
                return True
            elif func_id == TransportConfig.HEART_RESP_FID:
                logger.debug("Receive %s:%s a HeartBeat Response" % (socket_session.get_ip(),socket_session.get_port()))
                return True
            return False
        else:
            return False


class SocketSession(object):
    """
    socket会话,处理收发数据及心跳协议
    SocketServer的子类使用时候，需要传入对应的子类parcle
    目前默认socket接收缓冲区为64k,发送缓冲区16K
    """
    def __init__(self, socket_session, address, parcel, func_write, func_read, func_offline=None, keep_heart_flag=TransportConfig.HEART_KEEP_IDLE):
        """
        @:param socket_session: socket 客户端socket
        @:param address: (ip,port) 客户端地址
        @:param parcel: BaseParcel 需要传入子类的parcle
        @:param func_write: 发送完所有队列数据后的回调，这里作用与SocketClient的func_write不同，目前可以传入None
        @:param func_read: 解析完一个完整包体协议后的处理回调
        @:param func_offline:断开连接回调函数
        @:param keep_heart_flag:是否启用默认配置心跳
        """
        self._socket = socket_session
        self._address = address
        self._cb_write = func_write
        self._cb_read = func_read
        self._cb_offlne = func_offline
        self.keepHeart_flag = keep_heart_flag

        # 一些配置
        self.__recv_buf_size = TransportConfig.READ_BUFFER_SIZE
        self.__send_buf_size = TransportConfig.SEND_BUFFER_SIZE
        self.__max_post_buf_size = TransportConfig.MAX_POST_BUFFER_SIZE

        # 设置发送缓冲区大小（16K），这里进行了预先申请，避免后面频繁动态申请
        send_buf_type = struct.Struct("!%us" % self.__send_buf_size)
        self.__send_buf = ctypes.create_string_buffer(send_buf_type.size)
        self.__send_position = 0

        # 待发送队列总缓冲数据量，不能超过__max_post_buf_size大小
        self.__total_buf = 0

        # 写对列数据
        self.__write_queue = Queue()
        self._is_alive = False

        self._Message = None
        self.set_message(parcel)

        # 心跳定时器
        self._timer = Timer(TransportConfig.HEART_KEEP_IDLE, 0)
        # 接收到数据的时间
        self.__recv_time = time.time()
        # session_id作为唯一标识
        self.session_id = Utils.get_new_request_id()

    def set_message(self, parcel):
        self._Message = Message(parcel)

    def get_ip(self):
        return self._address[0]

    def get_port(self):
        return self._address[1]

    def get_msgtool(self):
        return self._Message

    def start(self):
        self._is_alive = True
        gevent.spawn(self._read_data)
        gevent.spawn(self._write_data)
        # 启动心跳
        if self.keepHeart_flag:
            self._timer.post(Runnable(self._start_hearbeat))

    def post_data(self, data):
        if self._is_alive:
            while (self.__total_buf + len(data)) > self.__max_post_buf_size:
                logger.warning("To be send buffer is too large,more than %u! wait 1s to post data" % self.__max_post_buf_size)
                gevent.sleep(1)
            self.__total_buf += len(data)
            self.__write_queue.put(data)
        else:
            logger.error("Client(IP:%s,Port:%d) is disconnect" % self._address)

    def send_data(self, system_region, msg_type, request_id, content):
        """
        主动调用发送数据
        :param system_region: 系统区号
        :param msg_type: 包子类型
        :param request_id: 请求ID
        :param content: 包体
        """
        data_resp_bytes = self._Message.pack_message(system_region, msg_type, request_id, content)
        self.post_data(data_resp_bytes)

    def stop(self):
        self._socket.close()
        self._is_alive = False
        if self.keepHeart_flag:
            self._timer.stop()

    def _read_data(self):
        while True:
            # 需要循环读包,一次读取64K，windows下测试一口气最大读入24464个字节
            try:
                data_buf = self._socket.recv(self.__recv_buf_size)
                # 设置接收时间
                self.__recv_time = time.time()
                if data_buf == "":
                    logger.error("Client(IP:%s,Port:%d) is disconnect" % self._address)
                    if self._cb_offlne is not None:
                        self._cb_offlne()
                    break
            except Exception,e:
                    logger.error("Client(IP:%s,Port:%d) is disconnect" % self._address)
                    logger.error("Disconnect reason:%s" % e)
                    if self._cb_offlne is not None:
                        self._cb_offlne()
                    break
            self._unpack_data(data_buf)

    def _write_data(self):
        while True:
            self.__send_position = 0
            # 队列为空时候，通过queue的get方法进行等待，socket_client里面的等待机制是通过等待锁实现
            __data = self.__write_queue.get()
            temp_data_size = len(__data)
            self.__total_buf -= temp_data_size

            # 第一条数据小于__send_buf_size打包到缓冲中，__send_position移动，大于它，直接发送出去
            if temp_data_size < self.__send_buf_size:
                struct.pack_into("%ss" % temp_data_size, self.__send_buf, 0, __data[0:temp_data_size])
                self.__send_position = temp_data_size
            else:
                try:
                    self._socket.sendall(__data)
                except Exception,e:
                    break

            # 开始发送队列中数据
            while not self.__write_queue.empty():
                __data = self.__write_queue.get()
                temp_data_size = len(__data)
                self.__total_buf -= temp_data_size
                # 当前发送缓冲__send_buf和去除数据大小之和小于__send_buf_size（16K）,将数据继续存入__send_buf，__send_position移动
                if (self.__send_position + temp_data_size) < self.__send_buf_size:
                    struct.pack_into("%ss" % temp_data_size, self.__send_buf, self.__send_position, __data[0:temp_data_size])
                    self.__send_position += temp_data_size
                # 当前发送缓冲__send_buf和取出数据大小之和大于__send_buf_size（16K），从取出数据截取部分数据和__send_buf组成
                # __send_buf_size大小，进行发送，发送完成之后，开始新的一轮，将剩余数据重新打入__send_buf，__send_position移动
                else:
                    temp_last_size = (self.__send_buf_size - self.__send_position)
                    struct.pack_into("%ss" % temp_last_size, self.__send_buf, self.__send_position, __data[0:temp_last_size])
                    try:
                        self._socket.sendall(self.__send_buf)
                        # 发送__send_buf完成需要将标志位置0，modify 20160301
                        self.__send_position = 0
                    except Exception, e:
                        break
                    tem_last_rest_size = temp_data_size - temp_last_size
                    if tem_last_rest_size < self.__send_buf_size:
                        struct.pack_into("%ss" % tem_last_rest_size, self.__send_buf, 0, __data[temp_last_size:])
                        self.__send_position = tem_last_rest_size
                    else:
                        # 剩余大小依然大于16K，直接一口气发送出去,modify 20160301
                        try:
                            self._socket.sendall(__data[temp_last_size:])
                        except Exception, e:
                            break
            # 队列发送完成后，处理剩余未足16K数据
            if self.__send_position != 0:
                try:
                    self._socket.sendall(self.__send_buf[0:self.__send_position])
                    # 发送__send_buf完成需要将标志位置0，modify 20160301
                    self.__send_position = 0
                except Exception, e:
                    logger.error("Client(IP:%s,Port:%d) is disconnect" % self._address)
                    break
            if self._cb_write is not None:
                self._cb_write()

    def _unpack_data(self, data_buf):
        """
        对每次读到的数据进行解包
        """
        total_size = len(data_buf)
        read_size = 0
        while read_size < total_size:
            read_buf = data_buf[read_size:total_size]
            try:
                read_size += self._Message.fill_buf(read_buf)
            except MsgParcelException,e:
                logger.error("Client(IP:%s,Port:%d) send wrong message:%s" % (self._address[0], self._address[1], e.get_msg()))
                self.stop()
                break
            except Exception, e:
                logger.error("%s:_unpack_data:Socket Data is not support.error:%s" % (self.__class__.__name__, e))
                self.stop()
                break
            if self._Message.Finish_flag:
                self._cb_read(self, self._Message.Content, self._Message.System_Region, self._Message.Msg_Type, self._Message.Request_Id)
                self._Message.clear()

    def _send_heartbeat(self):
        """
        发送心跳包
        """
        data_resp_bytes = self._Message.pack_message(TransportConfig.HEART_SYSTEM_REGION,TransportConfig.HEART_REQ_FID, Utils.get_new_request_id(), None)
        self.post_data(data_resp_bytes)
        logger.debug("Send a HeartBeat Request")

    def _start_hearbeat(self):
        """
        开始进行心跳监测
        """
        if not self._is_alive:
            logger.error("The heart beat failed,Because The Socket Client disconneted!")
        else:
            endtime = time.time()
            latency = endtime - self.__recv_time
            if latency < TransportConfig.HEART_KEEP_IDLE:
                self._timer.reset((TransportConfig.HEART_KEEP_IDLE - latency), 0)
                self._timer.post(Runnable(self._start_hearbeat))
            elif TransportConfig.HEART_KEEP_IDLE <= latency < (TransportConfig.HEART_KEEP_IDLE + TransportConfig.HEART_KEEP_INTERVAL):
                self._send_heartbeat()
                self._timer.reset(TransportConfig.HEART_KEEP_IDLE, 0)
                self._timer.post(Runnable(self._start_hearbeat))
            else:
                # 停止timer
                self._timer.stop()
                self.stop()
