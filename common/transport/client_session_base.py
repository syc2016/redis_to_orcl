#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
from common.thread.handler_thread import Runnable, Timer
from common.utils import Utils
from config import TransportConfig
from socket_client import SocketClient
import uuid
import time
from common.log.manager import LogManager
logger = LogManager.register_log("client_session_base")


class ClientSessionBase(object):
    """
    包装SocketClient，在该层处理心跳协议,以及对网络状态的改变进行回调处理
    """
    def __init__(self, ip, port, func_online=None, keep_heart_flag=TransportConfig.KEEP_ALIVE_FLAG):
        """
        :param ip: socket IP
        :param port:socket Port
        :param func_online:网络状态改变回调
        :param keep_heart_flag:是否启用默认配置心跳
        """
        self._ip = ip
        self._port = port
        # session_id作为唯一标识
        self.session_id = Utils.get_new_request_id()
        # 网络连接成功与断开的回调
        self._cb_online = func_online
        # socket连接服务器客户端
        self._socket_client = None
        self.set_socket_client(ip, port)
        self.keepHeart_flag = keep_heart_flag
        # 心跳定时器
        self._timer = Timer(TransportConfig.HEART_KEEP_IDLE, 0)

    def set_socket_client(self, ip, port):
        self._socket_client = SocketClient(ip, port, self._create_parcel(), self._cb_write, self._cb_read, self._cb_offline)

    def get_ip(self):
        return self._ip

    def get_port(self):
        return self._port

    def get_alive(self):
        return self._socket_client.get_alive()

    def start(self):
        """
        开启客户端连接
        """
        return self._socket_client.start()

    def stop(self, flag=False):
        """
        停止客户端连接
        @:param flag:True彻底销毁socketclient资源，false时候只会断开socket，稍后会自动进行重连
        :return:
        """
        self._socket_client.stop(flag)
        if flag and self._timer is not None:
            self._timer.stop()

    def send_data(self, system_region, msg_type, request_id, content):
        """
        主动调用发送数据，并通知发送IO就绪
        :param system_region: 系统区号
        :param msg_type: 包子类型
        :param request_id: 请求ID
        :param content: 包体
        """
        self._socket_client.post_raw_data(system_region, msg_type, request_id, content)
        self._socket_client.notify_write()

    def _handle_heartbeat(self, request_id, system_id=TransportConfig.HEART_SYSTEM_REGION):
        """
        处理心跳请求
        """
        self._socket_client.post_raw_data(system_id, TransportConfig.HEART_RESP_FID, request_id, None)
        self._socket_client.notify_write()
        logger.debug("Handle a HeartBeat Request from IP:%s,Port:%s and Send a HeartBeat Response.request_id:%s" % (self._ip, self._port, uuid.UUID(bytes=request_id)))

    def _create_parcel(self):
        """
        创建解析工具，需要由子类继承
        """
        raise AssertionError('%s: _create_parcel must be implemente!' % self.__class__.__name__)

    def _cb_write(self):
        """
        IO可用时候回调，进行组包发送，将多个小协议体组建成一个协议体
        这里对客户端本身性能影响不大，主要影响连接的服务端对处理包体的性能，
        比如下单笔和下批量包，服务单对批量包就是进行一口气处理
        因此这里子类可以根据具体情况并不一定需要实现该方法
        :return:
        """
        pass

    def _cb_read(self, data, system_region, func_id, request_id):
        """
        读取到完整消息体时回调处理,需要被子类继承继续处理
        :param data:消息体
        :param system_region:系统区号
        :param func_id:子包类型
        :param request_id:请求id
        :return:
        """
        if self.keepHeart_flag:
            if func_id == TransportConfig.HEART_REQ_FID:
                # 返回心跳响应
                self._handle_heartbeat(request_id, system_region)
                return True
            elif func_id == TransportConfig.HEART_RESP_FID:
                # 心跳响应不用进行任何处理
                logger.debug("Receive a HeartBeat Response from IP:%s,Port:%s.request_id:%s" % (self._ip, self._port, uuid.UUID(bytes=request_id)))
                return True
            return False
        else:
            return False

    def _cb_offline(self, ret_code=TransportConfig.NO_ERROR, system_id=TransportConfig.HEART_SYSTEM_REGION):
        """
        网络连接状态改变回调,这个方法用于上层回调通知，在往上传通知通过_cb_online
        断开时候，需要进行相关资源的清空，该方法需要被子类继承继续处理
        """
        if self._cb_online is not None:
            self._cb_online(ret_code)
        if ret_code == TransportConfig.NO_ERROR:
            logger.info("The Server(IP:%s Port:%s) is on line" % (self._ip, self._port))
            # 开始启动心跳定时器
            if self.keepHeart_flag:
                self._timer.reset()
                self._timer.post(Runnable(self._start_hearbeat, system_id))

    def _send_heartbeat(self, system_id=TransportConfig.HEART_SYSTEM_REGION):
        """
        发送心跳包
        该方法在开启协议心跳包时有效
        """
        request_id = uuid.uuid1()
        self._socket_client.post_raw_data(system_id, TransportConfig.HEART_REQ_FID, request_id.bytes, None)
        self._socket_client.notify_write()
        logger.debug("Send a HeartBeat Request to IP:%s,Port:%s,request_id:%s" % (self._ip, self._port, request_id))

    def _start_hearbeat(self, system_id=TransportConfig.HEART_SYSTEM_REGION):
        """
        开始进行心跳监测
        该方法在开启协议心跳包时有效
        """
        if not self._socket_client.get_alive():
            logger.error("The heart beat failed,Because The Socket IP:%s,Port:%s Client disconneted!" % (self._ip, self._port))
        else:
            endtime = time.time()
            latency = endtime - self._socket_client.get_recv_time()
            if latency < TransportConfig.HEART_KEEP_IDLE:
                self._timer.reset((TransportConfig.HEART_KEEP_IDLE - latency), 0)
                self._timer.post(Runnable(self._start_hearbeat, system_id))
                logger.debug("_start_hearbeat:IP:%s,Port:%s find latency<%s and reset timer and heartbeat" % (self._ip, self._port, TransportConfig.HEART_KEEP_IDLE))
            elif TransportConfig.HEART_KEEP_IDLE <= latency < (TransportConfig.HEART_KEEP_IDLE + TransportConfig.HEART_KEEP_INTERVAL):
                self._send_heartbeat(system_id)
                self._timer.reset(TransportConfig.HEART_KEEP_IDLE, 0)
                self._timer.post(Runnable(self._start_hearbeat, system_id))
                logger.debug("_start_hearbeat:IP:%s,Port:%s find %s<=latency<%s and send a heartbeat" %
                             (self._ip, self._port, TransportConfig.HEART_KEEP_IDLE, (TransportConfig.HEART_KEEP_IDLE + TransportConfig.HEART_KEEP_INTERVAL)))
            else:
                # 停止timer
                self._timer.stop()
                self._socket_client.stop()
                logger.debug("_start_hearbeat:IP:%s,Port:%s find latency>=%s and close the socket" % (self._ip, self._port, TransportConfig.HEART_KEEP_IDLE + TransportConfig.HEART_KEEP_INTERVAL))




