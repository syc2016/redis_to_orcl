#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
import struct
import ctypes
import uuid
from config import TransportConfig


class Message(object):
    """
    包体目前分配了1000个字节，这个值可以看实际情况在定一个合适的值，目前是避免频繁申请内存
    _head_buf:包头(字节流字符串形式)
    _content_buf：内容体(字节流字符串形式)
    _spare_content_buf：备用缓冲区，超出指定大小时候启用
    _spare_content_flag：备用缓冲区启用标志
    _head_format_type：包头类型
    _buf_format_type：包体和包体类型
    _head_size_type：用于包头分配长度
    _head_buf_size：包头长度
    完整的消息包结构
    Content_Length：包体长度
    System_Region：系统区号
    Msg_Type：子包类型
    Request_Id：请求id
    Content: 内容体（已经转换成具体的协议类型）
    __head_finish: 包头是否解析完成
    Finish_flag: 消息是否全部解析完成
    """
    def __init__(self, parcel):
        """
        构造方法
        :param parcel: BaseParcel及其子类,解析包体
        """
        self.__parcel = parcel

        # 以下包头类型可由set_buf_format_type去复写类型
        self._head_format_type = None
        self._buf_format_type = None
        self._head_size_type = None
        self._head_buf_size = 0
        self.set_buf_format_type()
        # （解包使用--读数据)预先分配24个字节给包头使用
        self.__head_buf_type = struct.Struct(self._head_size_type)
        self._head_buf = ctypes.create_string_buffer(self.__head_buf_type.size)
        # 包头指针位置
        self.__head_buf_position = 0

        # 解包使用--读数据)预先分配1000个字节给包体使用，如果包体超出此大小，在重新进行分配
        self.__content_buf_type = struct.Struct("!1000s")
        self._content_buf = ctypes.create_string_buffer(self.__content_buf_type.size)
        # 包体指针位置
        self.__content_buf_position = 0
        # 备用的缓冲区
        self._spare_content_buf = None
        self._spare_content_flag = False

        self.Content_Length = None
        self.System_Region = None
        self.Msg_Type = None
        self.Request_Id = None
        self.Content = None

        self.Finish_flag = False
        self.__head_finish = False

    def set_buf_format_type(self):
        """
        设置消息体类型，可由子类继承复写
        """
        self._head_format_type = "!IHH16s"
        self._buf_format_type = "!IHH16s%ss"
        self._head_size_type = "!24s"
        self._head_buf_size = 24

    def _get_content_buf(self):
        """
        当包体数据超出我们预先分配的__content_buf_type大小
        我们创建备用的缓冲区，使用完后释放
        :return:
        """
        if self._spare_content_flag:
            return self._spare_content_buf
        else:
            return self._content_buf

    def _unpack_head(self):
        """
        解析包头
        """
        # unpack比unpack_from效率略高一些，这里直接用unpack
        self.Content_Length, self.System_Region, self.Msg_Type, self.Request_Id = struct.unpack(self._head_format_type, self._head_buf)
        self.__head_finish = True
        # 包头刚解析完成，为防止上次操作完没有clear，这里手动清空包体指针位置为0
        self.__content_buf_position = 0
        if self.Content_Length > TransportConfig.MAX_BUFFER_SIZE:
            raise PackageParcelException('package size %s out of range.Default max size:%s' % (self.Content_Length, TransportConfig.MAX_BUFFER_SIZE))
        # 判断包体大小是否超过默认大小,超过默认大小，动态分配包体长度的内存，解析完成后释放
        if self.Content_Length > self.__content_buf_type.size:
            self._spare_content_flag = True
            spare_content_buf_type = struct.Struct("!%ss" % self.Content_Length)
            self._spare_content_buf = ctypes.create_string_buffer(spare_content_buf_type.size)
        else:
            self._spare_content_flag = False

    def _unpack_content(self):
        """
        解析包体内容
        """
        self.__parcel.unpack_content(self, self.__content_buf_position)
        self.Finish_flag = True
        # 包体刚解析完成，为防止上次操作完没有clear，这里手动清空包头指针位置为0
        self.__head_buf_position = 0

    def pack_message(self, system_region, msg_type, request_id, content):
        """
        对消息进行整体打包，返回消息字节流
        打包数据不能和读方式一样采用预申请内存方式，因为读完一个完整包就会立刻
        回调处理，而打完一个完整包并不会立刻发送出去，可能使用它来多次打包数据，
        所以此块内存必须动态申请
        """
        if content is not None:
            temp_content_bytes = content.SerializeToString()
            temp_content_length = len(temp_content_bytes)
            data_buf = struct.pack(self._buf_format_type % temp_content_length,temp_content_length,
                                   system_region, msg_type, request_id, temp_content_bytes)
        else:
            # content为None，表示发送的消息体为空
            data_buf = struct.pack(self._head_format_type, 0, system_region, msg_type, request_id)
        return data_buf

    def fill_buf(self, buf):
        """
        将buf进行单个pb协议的解析
        先解析包头，后解析包体
        返回当前解析完成后读取的buf中的的字节数
        """
        # 传入的字节长度
        temp_size = len(buf)
        # 包头已经解析完成
        if self.__head_finish:
            if (temp_size + self.__content_buf_position) >= self.Content_Length:
                temp_read_size = self.Content_Length - self.__content_buf_position
                temp_buf = buf[0:temp_read_size]
                struct.pack_into("%ss" % temp_read_size, self._get_content_buf(), self.__content_buf_position, temp_buf)
                self.__content_buf_position += temp_read_size
                try:
                    self._unpack_content()
                except MsgParcelException, e:
                    e.size = temp_read_size
                    raise e
                return temp_read_size
            else:
                struct.pack_into("%ss" % temp_size, self._get_content_buf(), self.__content_buf_position, buf)
                self.__content_buf_position += temp_size
                return temp_size
        # 包头未解析完成
        else:
            # 传入的buf和原来已有的包头缓冲足够构成一个完整的包头
            if (temp_size + self.__head_buf_position) >= self._head_buf_size:
                temp_read_size = self._head_buf_size-self.__head_buf_position
                temp_buf = buf[0:temp_read_size]
                struct.pack_into("%ss" % temp_read_size, self._head_buf, self.__head_buf_position, temp_buf)
                self.__head_buf_position += temp_read_size
                self._unpack_head()
                temp_last_size = temp_size - temp_read_size
                # 填充完包头后，剩余数据足够填充一个完整的包体
                if temp_last_size >= self.Content_Length:
                    temp_last_size -= self.Content_Length
                    temp_buf = buf[temp_read_size:(temp_read_size + self.Content_Length)]
                    struct.pack_into("%ss" % self.Content_Length, self._get_content_buf(), self.__content_buf_position, temp_buf)
                    self.__content_buf_position += self.Content_Length
                    try:
                        self._unpack_content()
                    except MsgParcelException, e:
                        e.size = temp_size - temp_last_size
                        raise e
                    return temp_size - temp_last_size
                # 填充完包头后，剩余数据不够填充一个完整的包体
                else:
                    temp_buf = buf[temp_read_size:(temp_read_size+temp_last_size)]
                    struct.pack_into("%ss" % temp_last_size, self._get_content_buf(), self.__content_buf_position, temp_buf)
                    self.__content_buf_position += temp_last_size
                    return temp_size
            # 传入的buf和原来已有的包头缓冲不够构成一个完整的包头，将数据全部读入包头
            else:
                struct.pack_into("%ss" % temp_size, self._head_buf, self.__head_buf_position, str(buf))
                self.__head_buf_position += temp_size
                return temp_size

    def clear(self):
        """
        清空所有内容
        """
        self.__head_buf_position = 0
        self.__content_buf_position = 0
        self._spare_content_buf = None
        self._spare_content_flag = False
        self.Content_Length = None
        self.System_Region = None
        self.Msg_Type = None
        self.Request_Id = None
        self.Content = None
        self.Finish_flag = False
        self.__head_finish = False


class MsgParcelException(Exception):
    """
    解析pb包数据异常,发生PB包无法被我们系统内部识别
    """
    def __init__(self, system_region, msg_type, request_id, error='UnKnown'):
        self.__msg = "system_region:%s,msg_type:%s,request_id:%s,can not unpack_content by Parcel.error:%s" % \
                     (hex(system_region), hex(msg_type), uuid.UUID(bytes=request_id), error)
        # 扩展属性，为了返回已读的数据长度
        self.size = 0

    def get_msg(self):
        return self.__msg


class PackageParcelException(Exception):
    """
    数据包本身存在异常，无法解析成一个正常PB
    """
    pass


class BaseParcel(object):
    """
    协议解析工具,需要被子类继承
    """
    # 将获取的包体字节流转换成对应的协议对象
    def unpack_content(self, msg, content_size):
        raise AssertionError('%s: unpack_content must be implemente!' % self.__class__.__name__)
