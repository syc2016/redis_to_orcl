#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'
import gevent
from gevent.queue import Queue
from gevent.event import Event
import gevent.monkey
gevent.monkey.patch_socket()
from gevent import socket
import ctypes
from time import time
import struct
from config import TransportConfig
from parcel_tool import MsgParcelException, PackageParcelException, Message
from common.log.manager import LogManager
logger = LogManager.register_log("socket_client")


class SocketClient(object):
    """
    socket客户端，内部类不需要被外部模块使用
    目前默认socket接收缓冲区为64k,发送缓冲区16K
    接收数据队列最大1000M，超过给予警告
    socket连接与读数据操作放在一个协程中，第一次连接成功会开启写协程，
    之后断开连接，写协程挂起，等待重新连接后释放锁，继续工作
    """
    def __init__(self, ip, port, parcel, func_write, func_read, func_offline):
        """
        :param ip:客户端IP
        :param port:客户端端口
        :param parcel:子类解析包体工具
        :param func_write:IO可用时候回调，主要用于组包操作,可以理解成该回调用于填充发送队列
        :param func_read: 解析成具体协议后的处理回调
        :param func_offline:网络状态改变回调
        """
        # IO可以进行发送数据时候的回调(写协程获取调度权)，用来组包
        self._cb_write = func_write
        # 读取到一个完整pb协议包的回调
        self._cb_read = func_read
        # 当网络状态改变时候回调
        self._cb_offline = func_offline
        # 当发送数据协程socket发送异常（断开连接），该锁会进行等待，重连后释放
        self.__writing_wait_event = Event()
        # 发送队列有数据通知该等待锁
        self.__wait_event = Event()

        # 一些配置
        self.__recv_buf_size = TransportConfig.READ_BUFFER_SIZE
        self.__send_buf_size = TransportConfig.SEND_BUFFER_SIZE
        self.__max_post_buf_size = TransportConfig.MAX_POST_BUFFER_SIZE

        # 写对列数据
        self.__write_queue = Queue()

        # 设置发送缓冲区大小（16K），这里进行了预先申请，避免后面频繁动态申请
        # 如果直接每次弹出队列数据发送的话，有可能小数据太多，频繁发送，所以
        # 进行了组装
        send_buf_type = struct.Struct("!%us" % self.__send_buf_size)
        self.__send_buf = ctypes.create_string_buffer(send_buf_type.size)
        self.__send_position = 0

        # 待发送队列总缓冲数据量，不能超过__max_post_buf_size大小
        self.__total_buf = 0

        # socket 延迟到_connect中创建实例，方便重连设置
        self._socket = None
        # 判断当前是否还处于正常连接状态
        self._is_alive = False
        # 服务器IP和端口
        self._ip = ip
        self._port = port
        # PB协议包解析工具
        self._Message = None
        self.set_message(parcel)

        # 接收到数据的时间
        self.__recv_time = time()

        # 是否销毁该socket的所有资源，包括读写协程
        self.__dispose_flag = False

    def set_message(self, parcel):
        self._Message = Message(parcel)

    def _write_data(self):
        """
        socket写数据，在写协程中工作
        分三个步骤:
        1.准备阶段：判断是否退出写协程，判断队列中是否有数据,是否进行等待，进行外部组包回调
        2.发送队列中的数据：进行组16k数据包发送
        3.队列发送完成后，处理剩余未足16K数据
        """
        while True:
            # 1.准备阶段
            self.__send_position = 0
            # 直接进入判断，如果设置了销毁，直接退出写协程
            if self.__dispose_flag:
                break
            # 尝试进行组包
            if self._cb_write is not None:
                self._cb_write()
            # 队列中还未有数据，进行等待
            if self.__write_queue.qsize() == 0:
                self.__wait_event = Event()
                self.__wait_event.wait()
                # 接收到发送数据信号，重新组包
                if self._cb_write is not None:
                    self._cb_write()
                if self.__write_queue.qsize() == 0:
                    continue

            # 2.发送队列中的数据
            while not self.__write_queue.empty():
                __data = self.__write_queue.get()
                temp_data_size = len(__data)
                self.__total_buf -= temp_data_size
                # 当前发送缓冲__send_buf和去除数据大小之和小于__send_buf_size（16K）,将数据继续存入__send_buf，__send_position移动
                if (self.__send_position + temp_data_size) < self.__send_buf_size:
                    struct.pack_into("%ss" % temp_data_size, self.__send_buf, self.__send_position, __data[0:temp_data_size])
                    self.__send_position += temp_data_size
                # 当前发送缓冲__send_buf和取出数据大小之和大于__send_buf_size（16K），从取出数据截取部分数据和__send_buf组成
                # __send_buf_size大小，进行发送，发送完成之后，开始新的一轮，将剩余数据重新打入__send_buf，__send_position移动
                else:
                    temp_last_size = (self.__send_buf_size - self.__send_position)
                    struct.pack_into("%ss" % temp_last_size, self.__send_buf, self.__send_position, __data[0:temp_last_size])
                    try:
                        self._socket.sendall(self.__send_buf)
                        # 发送__send_buf完成需要将标志位置0，modify 20160301
                        self.__send_position = 0
                    except Exception, e:
                        break
                    # 发送完成后的剩余数据长度
                    tem_last_rest_size = temp_data_size - temp_last_size
                    # 剩余数据长度超出16K,直接发送出去，不需要在组装
                    if tem_last_rest_size >= self.__send_buf_size:
                        try:
                            self._socket.sendall(__data[temp_last_size:])
                        except Exception, e:
                            break
                    # 剩余数据未超出16K,等待下轮组装
                    else:
                        struct.pack_into("%ss" % tem_last_rest_size, self.__send_buf, 0, __data[temp_last_size:])
                        self.__send_position = tem_last_rest_size

            # 3.队列发送完成后，处理剩余未足16K数据
            if self.__send_position != 0:
                try:
                    self._socket.sendall(self.__send_buf[0:self.__send_position])
                    # 发送__send_buf完成需要将标志位置0，modify 20160301
                    self.__send_position = 0
                except Exception, e:
                    if self.__dispose_flag:
                        break
                    self.__writing_wait_event = Event()
                    self.__writing_wait_event.wait()
                    continue

    def _read_data(self):
        """
        socket读数据处理,和socket连接处于一个协程中工作
        我们规定重连也只能由该协程发起
        """
        while True:
            # 需要循环读包,目前暂时一次读取__recv_buf_size长度，后面可看具体性能调整大小
            try:
                data_buf = self._socket.recv(self.__recv_buf_size)
                # 启用协议心跳包，一旦接收到数据，就重新设置接收时间
                if TransportConfig.KEEP_ALIVE_FLAG:
                    self.__recv_time = time()
                if data_buf == "":
                    self._disconnect()
                    break
            except Exception as e:
                    self._disconnect()
                    break
            self._unpack_data(data_buf)

    def _unpack_data(self, data_buf):
        """
        对每次读到的数据进行解包
        """
        total_size = len(data_buf)
        read_size = 0
        # 用于进行协程的切换统计
        temp_compare_size = TransportConfig.HANDLE_DATA_SIZE
        while read_size < total_size:
            read_buf = data_buf[read_size:total_size]
            try:
                read_size += self._Message.fill_buf(read_buf)
                if self._Message.Finish_flag:
                    self._cb_read(self._Message.Content, self._Message.System_Region, self._Message.Msg_Type, self._Message.Request_Id)
                    self._Message.clear()
                if read_size > temp_compare_size:
                    temp_compare_size = temp_compare_size + TransportConfig.HANDLE_DATA_SIZE
                    gevent.sleep(0)
            except MsgParcelException, e:
                logger.error(e.get_msg())
                self._Message.clear()
                read_size += e.size
                continue
            except PackageParcelException, e:
                logger.error(e.message)
                self._Message.clear()
                self._socket.close()
                break
            except Exception, e:
                logger.exception("%s:_unpack_data:Socket Data is not support.error:%s" % (self.__class__.__name__, e))
                self._Message.clear()
                self._socket.close()
                break

    def start(self):
        """
        启动客户端，先进行连接，连接成功，开启读写协程
        """
        gevent.spawn(self._connect)
        self.__dispose_flag = False

    def notify_write(self):
        """
        唤醒发送数据操作
        """
        if self._is_alive:
            self.__wait_event.set()
        else:
            self._cb_offline(TransportConfig.ERROR_SERVICE_OFFLINE)

    def stop(self, dispose_flag=False):
        """
        关闭客户端连接
        该方法由外部调用，我们默认只有读协程有权利进行断开重连
        这样能保证socket客户端一直只有两个协程(连接并且读数据和写数据)
        这里外部调用时候我们只进行原始socket的断开连接，然后在由内部
        读协程捕获读取异常，进行内部的_disconnect调用，并且尝试重连
        dispose_flag:默认False不会销毁读写线程，True则销毁读写线程，彻底销毁socke的所有资源
        """
        if dispose_flag:
            # 这里设置的顺序一定要先设置__dispose_flag，之后才能调用__writing_wait_event.set
            # 防止调用该方法前，socket已经断开，写线程被阻塞，所以需要先设置标志位
            self.__dispose_flag = True
            self.__writing_wait_event.set()
        if self._is_alive:
            self._socket.close()

    def get_alive(self):
        """
        获取客户端状态
        """
        return self._is_alive

    def get_recv_time(self):
        """
        启用协议心跳包时，最近一次接收到数据的时间
        """
        return self.__recv_time

    def post_data(self, data):
        """
        data已经是字节流数据，包含包头和包体
        """
        if self._is_alive:
            while (self.__total_buf + len(data)) > self.__max_post_buf_size:
                logger.warning("To be send buffer is too large,more than %u! wait 1s to post data" % self.__max_post_buf_size)
                gevent.sleep(1)
            self.__total_buf += len(data)
            self.__write_queue.put(data)
        else:
            logger.error("The Server(IP:%s Port:%s) is off line" % (self._ip, self._port))
            self._cb_offline(TransportConfig.ERROR_SERVICE_OFFLINE)

    def post_raw_data(self, system_region, msg_type, request_id, content):
        """
        将原始数据参数传入，内部构建
        """
        data_bytes = self._Message.pack_message(system_region, msg_type, request_id, content)
        self.post_data(data_bytes)

    def _connect(self):
        """
        所有客户端连接中，无论首次还是重连，始终跑在第一次开启的两个协程中
        连接服务器，连接和读数据都保持在一个协程中操作，写数据在另一个协程
        flag:默认True代表首次连接，False表示进行重连，此时不用再开发送数据协程
        """
        need_start_write_thread = True
        while True:
            while not self._is_alive:
                logger.info("start to connect the server,IP:%s,PORT:%s" % (self._ip, self._port))
                try:
                    # 如果外部是销毁，则不再进行重连
                    if self.__dispose_flag:
                        break
                    # 这里重新设置socket，主要是为了重连使用，否者socket无法重新连接上
                    self._socket = None
                    self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    # 如果采用TCP默认心跳包设置，采用下面方式设置保活
                    if not TransportConfig.KEEP_ALIVE_FLAG:
                        self.__set_tcp_keepalive()
                    self._socket.connect((self._ip, self._port))
                    logger.info("success to connect the server,IP:%s,PORT:%s" % (self._ip, self._port))
                except Exception, e:
                    self._is_alive = False
                    logger.error("can not connect the server,IP:%s,PORT:%s" % (self._ip, self._port))
                    logger.info("%ss later try to restart connect the server,IP:%s,PORT:%s" % (TransportConfig.RECONNECT_TIME, self._ip, self._port))
                    gevent.sleep(TransportConfig.RECONNECT_TIME)
                    continue
                # 启用协议心跳包，一旦连接成功，就重新设置接收时间
                if TransportConfig.KEEP_ALIVE_FLAG:
                    self.__recv_time = time()
                self._is_alive = True
                self._cb_offline()
                self.__writing_wait_event.set()
            # 如果外部是销毁，则不再进行任何操作
            if self.__dispose_flag:
                return
            if need_start_write_thread:
                gevent.spawn(self._write_data)
                need_start_write_thread = False
            self._read_data()

    def _disconnect(self):
        """
        内部的断开连接处理
        只由读协程调用
        flag:默认True，会进行断开重连，false则不进行断开重连
        """
        logger.error("Socket Client is disconnect,IP:%s,PORT:%s" % (self._ip, self._port))
        # 清空发送数据
        if self.__write_queue.qsize() > 0:
            self.__write_queue.get_nowait()
        # 清空消息解析工具
        self._Message.clear()
        self._socket.close()
        self._is_alive = False
        # 连接断开回调通知上层
        self._cb_offline(TransportConfig.ERROR_SERVICE_OFFLINE)

    def __set_tcp_keepalive(self):
        """
        采用TCP内部保活机制
        区分了windows平台和linux平台
        目前我们内部采用内部实现的心跳包，暂时保留该方法暂未使用
        """
        import platform
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
        sysstr = platform.system()
        if sysstr == "Windows":
            self._socket.ioctl(socket.SIO_KEEPALIVE_VALS, (1, 10000, 3000))
        elif sysstr == "Linux":
            self._socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 10)
            self._socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 3)
            self._socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 10)
