#!/usr/bin/env python
# coding: utf-8
__author__ = 'zmmgiser'


class TransportConfig(object):
    # 一个PB包允许的最大包大小
    MAX_BUFFER_SIZE = 1024 * 1024 * 100
    # 接收缓冲区64k
    READ_BUFFER_SIZE = 65536
    # 发送缓冲区16k
    SEND_BUFFER_SIZE = 16384
    # 待发送数据最大为1000M,超出给予打印警告提示
    MAX_POST_BUFFER_SIZE = 1048576000
    # 切换协程的处理数据大小阈值8K
    HANDLE_DATA_SIZE = 8192
    # 是否启用协议心跳
    KEEP_ALIVE_FLAG = True
    # 重连时间
    RECONNECT_TIME = 5
    # 获取空闲心跳间隔，多少s没有数据接收后进行发送心跳包
    HEART_KEEP_IDLE = 30
    # 获取心跳包发出后，多少s没有接收到数据或者心跳响应，表明服务器已经断开
    HEART_KEEP_INTERVAL = 30
    # 心跳系统区号
    HEART_SYSTEM_REGION = 0
    # 心跳请求包子类型
    HEART_REQ_FID = 0x0FFF
    # 心跳响应包子类型
    HEART_RESP_FID = 0x8FFF
    # 网络层出现断开时候传递的错误码
    ERROR_SERVICE_OFFLINE = 1
    # 连接成功，没有错误
    NO_ERROR = 0

